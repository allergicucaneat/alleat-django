import sys

import os
from django.db.utils import IntegrityError

sys.path.append(os.path.join(os.path.dirname(__file__), ''))

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "alleat_django.settings")

# your imports, e.g. Django models
from alleat_api.models.users import AUser
from alleat_api.models.instances import update_allergen_db, update_post_db


def create_superuser(email, password):
    return AUser.objects.create_superuser(email, password)


def create_user(email, password):
    return AUser.objects.create_user(email=email, password=password)


if __name__ == '__main__':

    print("Updating DB ...")
    print("Updating Allergens ...")
    update_allergen_db()
    print("Updating Posts ...")
    update_post_db()
    if len(sys.argv) == 4 and sys.argv[1] == 'addclient':
        print("Creating client user ...")
        try:
            user = create_user(sys.argv[2], sys.argv[3])
            print("Client successfully created!")
        except IntegrityError:
            print("Client user already exist!")

    elif len(sys.argv) == 4 and sys.argv[1] == 'addsuper':
        print("Creating admin user ...")
        try:
            user = create_superuser(sys.argv[2], sys.argv[3])
            print("Superuser successfully created!")
        except IntegrityError:
            print("Superuser already exist!")

    else:
        print("Usage: " + sys.argv[0] + " addsuper EMAIL PASSWORD")
        print("Usage: " + sys.argv[0] + " addclient EMAIL PASSWORD")
        exit(1)
