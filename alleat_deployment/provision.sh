#!/usr/bin/env bash

#
# ==================================================
# Provision shell script for X Django project
# ==================================================
#

SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
PROJECTPATH=$SCRIPTPATH/..
PROJECTNAME=alleat-django

PROVISION_RENDER=$PROJECTPATH/alleat_deployment/provision_render.py


update_apt_get () {
    sudo apt-get -y update
}

install_if_not_installed () {
    for prog in $@
    do
        echo "Installing $prog ..."
        output=$(apt-cache policy $prog)
        #echo $output
        if test "${output#*Installed: (none)}" != "$output"; then
            sudo apt-get update
            sudo apt-get -y install $prog
        else
            echo "$prog was already installed"
        fi
    done
}

install_project_dependencies () {
    install_if_not_installed git build-essential python-dev python-setuptools python-pip python-virtualenv \
    libjpeg-dev libpng12-dev python-imaging postgresql postgresql-contrib libpq-dev supervisor nginx

    test -f /usr/lib/libfreetype.so || sudo ln -s /usr/lib/`uname -i`-linux-gnu/libfreetype.so /usr/lib/
    test -f /usr/lib/libjpeg.so || sudo ln -s /usr/lib/`uname -i`-linux-gnu/libjpeg.so /usr/lib/
    test -f /usr/lib/libz.so || sudo ln -s /usr/lib/`uname -i`-linux-gnu/libz.so /usr/lib/
}

install_test_dependencies () {
    install_if_not_installed phantomjs
}

create_user_group () {
    USER=$1
    GROUP=$2

    echo 'Creating user/group ...'
    sudo groupadd --system $GROUP
    sudo useradd --system --gid $GROUP --shell /bin/bash --home /webapps/$USER $USER

    echo 'Making user directory ...'
    test -d /webapps/$USER || sudo mkdir -p /webapps/$USER
    sudo chown $USER:$GROUP /webapps/$USER
}

create_virtualenv () {
    USER=$1

    DEPLOY_REQUIREMENTS=$SCRIPTPATH/deploy_requirements.txt

    echo 'Creating virtualenv ...'
    test -f /webapps/$USER/bin/python || sudo su $USER -c "virtualenv /webapps/$USER"

    PIP=/webapps/$USER/bin/pip
    sudo $PIP install pip==8.1.2
    sudo $PIP install -r $DEPLOY_REQUIREMENTS

}

provision_test_virtualenv () {
    USER=$1

    echo 'Creating virtualenv ...'
    test -f /webapps/$USER/bin/python || sudo su $USER -c "virtualenv /webapps/$USER"

    PIP=/webapps/$USER/bin/pip
    sudo $PIP install pip==8.1.2
}

copy_and_set_up_django_project () {
    USER=$1
    SERVER_NAME=$2
    DB_NAME=$3
    DB_PASSWORD=$4
    DEBUG=$5
    PATH_TO_SOURCE_PACKAGE=$6

    DEST_DIR=/webapps/$USER/$PROJECTNAME
    SETTINGS_FILE=/webapps/$USER/$PROJECTNAME/alleat_django/settings.py
    STATIC_ROOT=/webapps/$USER/var/static/
    MEDIA_ROOT=/webapps/$USER/var/media/
    PIP=/webapps/$USER/bin/pip
    PYTHON=/webapps/$USER/bin/python
    MANAGE_PY=/webapps/$USER/$PROJECTNAME/manage.py

    echo 'Copying project directory to destination folder ...'
    test -d $DEST_DIR && sudo rm -R $DEST_DIR
    sudo su $USER -c "mkdir -p $DEST_DIR"
    sudo su $USER -c "$PIP install --target=$DEST_DIR $PATH_TO_SOURCE_PACKAGE"

    # Set settings.py file
    sudo su $USER -c "python $PROVISION_RENDER settings $STATIC_ROOT $MEDIA_ROOT $SERVER_NAME $DB_NAME $USER $DB_PASSWORD $DEBUG > $SETTINGS_FILE"

    echo 'Creating and copying static directory ...'
    test -d $STATIC_ROOT && sudo su $USER -c "rm -R $STATIC_ROOT"
    sudo su $USER -c "mkdir -p $STATIC_ROOT"
    sudo su $USER -c "$PYTHON $MANAGE_PY collectstatic --noinput"

    echo 'Creating media directory ...'
    test -d $MEDIA_ROOT || sudo su $USER -c "mkdir -p $MEDIA_ROOT"

}

set_up_gunicorn_supervisor () {
    USER=$1
    GROUP=$2
    PROGRAM=$3
    SETTINGS_FILE=$4

    SOCKFILE=/webapps/$USER/run/gunicorn.sock
    RUNDIR=$(dirname $SOCKFILE)
    LOG_FILE=/webapps/$USER/logs/gunicorn_supervisor.log
    LOGDIR=$(dirname $LOG_FILE)
    START_SCRIPT=/webapps/$USER/bin/gunicorn_start.sh
    DJANGO_PROJECT_DIR=/webapps/$USER/$PROJECTNAME/
    ACTIVATE_VIRTUALENV=/webapps/$USER/bin/activate
    SUPERVISOR_CONF=/etc/supervisor/conf.d/$PROGRAM.conf

    echo 'Setting up gunicorn ...'
    sudo su $USER -c "python $PROVISION_RENDER gunicorn_start $PROGRAM $DJANGO_PROJECT_DIR $SOCKFILE $LOG_FILE $USER $GROUP $ACTIVATE_VIRTUALENV > $START_SCRIPT"
    sudo su $USER -c "chmod a+x $START_SCRIPT"

    echo 'Setting up supervisor ...'
    sudo su $USER -c "test -d $RUNDIR || mkdir -p $RUNDIR"
    sudo su $USER -c "test -d $LOGDIR || mkdir -p $LOGDIR"
    sudo su $USER -c "test -f $LOGFILE || touch $LOGFILE"

    sudo python $PROVISION_RENDER supervisor $PROGRAM $START_SCRIPT $USER $LOG_FILE > $SUPERVISOR_CONF

    sudo supervisorctl reread
    sudo supervisorctl update
    sudo supervisorctl stop $PROGRAM
    sudo supervisorctl start $PROGRAM
}

set_up_nginx () {
    USER=$1
    PROGRAM=$2
    SERVER_NAME=$3
    PORT=$4

    SOCKFILE=/webapps/$USER/run/gunicorn.sock
    LOGDIR=/webapps/$USER/logs
    ACCESS_LOGFILE=$LOGDIR/nginx-access.log
    ERROR_LOGFILE=$LOGDIR/nginx-error.log
    STATIC=/webapps/$USER/var/static/
    MEDIA=/webapps/$USER/var/media/
    NGINX_SITE_NAME=nginx-site-$PROGRAM

    echo 'Setting up nginx ...'
    sudo su $USER -c "test -d $LOGDIR || mkdir -p $LOGDIR"
    sudo su $USER -c "test -f $ACCESS_LOGFILE || touch $ACCESS_LOGFILE"
    sudo su $USER -c "test -f $ERROR_LOGFILE || touch $ERROR_LOGFILE"

    sudo python $PROVISION_RENDER nginx $PROGRAM $SOCKFILE $PORT $SERVER_NAME $ACCESS_LOGFILE $ERROR_LOGFILE $STATIC $MEDIA > /etc/nginx/sites-available/$NGINX_SITE_NAME

    test -f /etc/nginx/sites-enabled/$NGINX_SITE_NAME && sudo rm /etc/nginx/sites-enabled/$NGINX_SITE_NAME
    sudo ln -s /etc/nginx/sites-available/$NGINX_SITE_NAME /etc/nginx/sites-enabled/$NGINX_SITE_NAME
    sudo service nginx restart
}

create_postgres_db () {
    USER=$1
    DATA_BASE_NAME=$2
    DB_PASSWORD=$3

    PYTHON=/webapps/$USER/bin/python
    MANAGE_PY=/webapps/$USER/$PROJECTNAME/manage.py
    DEST_DIR=/webapps/$USER/$PROJECTNAME

    echo 'Creating postgres db ...'
    sudo su postgres -c "createuser -s -r $USER > /dev/null 2>&1"
    sudo su postgres -c "psql -c \"ALTER USER $USER WITH ENCRYPTED PASSWORD '$DB_PASSWORD';\" > /dev/null 2>&1"
    sudo su $USER -c "createdb $DATA_BASE_NAME > /dev/null 2>&1"

    synchronize_and_migrate_db $USER
}

synchronize_and_migrate_db () {
    USER=$1

    PYTHON=/webapps/$USER/bin/python
    MANAGE_PY=/webapps/$USER/$PROJECTNAME/manage.py
    DEST_DIR=/webapps/$USER/$PROJECTNAME

    echo 'Synchronizating and migrating data base ...'
    cd $DEST_DIR
    sudo su $USER -c "$PYTHON $MANAGE_PY syncdb --noinput"
    sudo su $USER -c "$PYTHON $MANAGE_PY migrate"
}

fill_test_db () {
    USER=$1

    DEST_DIR=/webapps/$USER/$PROJECTNAME
    PYTHON=/webapps/$USER/bin/python

    sudo su $USER -c "$PYTHON $DEST_DIR/db_manage.py addsuper admin@123.com 123"
    sudo su $USER -c "$PYTHON $DEST_DIR/db_manage.py addclient client@123.com 123"
}

## Main
if [ "$1" = "build_package" ]; then
    echo "change to directory"
    cd $PROJECTPATH/
    sudo apt-get -y install python-setuptools
    python setup.py bdist_wheel

elif [ "$1" = "devel" ]; then
    PATH_TO_SOURCE_PACKAGE=$2

    USER=alleatdev
    GROUP=webapps
    APP=alleat_devel
    SERVER_NAME=devel.allergicucaneat.com
    SERVER_PORT=80
    SETTINGS_FILE=settings_devel.py
    DATA_BASE_NAME=sqldb
    DB_PASSWORD=password
    DEBUG=True

    install_project_dependencies
    #install_test_dependencies
    create_user_group $USER $GROUP
    create_virtualenv $USER
    #provision_test_virtualenv $USER
    copy_and_set_up_django_project $USER $SERVER_NAME $DATA_BASE_NAME $DB_PASSWORD $DEBUG $PATH_TO_SOURCE_PACKAGE
    create_postgres_db $USER $DATA_BASE_NAME $DB_PASSWORD
    fill_test_db $USER
    set_up_gunicorn_supervisor $USER $GROUP $APP
    set_up_nginx $USER $APP $SERVER_NAME $SERVER_PORT

elif [ "$1" = "test" ]; then
    PATH_TO_SOURCE_PACKAGE=$2

    USER=alleattst
    GROUP=webapps
    APP=alleat_test
    SERVER_NAME=test.allergicucaneat.com
    SERVER_PORT=80
    DATA_BASE_NAME=sqldb_test
    DB_PASSWORD=Lz633KjvVjHR75a
    DEBUG=True

    install_project_dependencies
    #install_test_dependencies
    create_user_group $USER $GROUP
    create_virtualenv $USER
    #provision_test_virtualenv $USER
    copy_and_set_up_django_project $USER $SERVER_NAME $DATA_BASE_NAME $DB_PASSWORD $DEBUG $PATH_TO_SOURCE_PACKAGE
    create_postgres_db $USER $DATA_BASE_NAME $DB_PASSWORD
    fill_test_db $USER
    set_up_gunicorn_supervisor $USER $GROUP $APP
    set_up_nginx $USER $APP $SERVER_NAME $SERVER_PORT

elif [ "$1" = "demo" ]; then
    USER=alleatdemo
    GROUP=webapps
    APP=alleat_demo
    SERVER_NAME=demoapp.allergicucaneat.com
    SERVER_PORT=80
    DATA_BASE_NAME=sqldb_alleat_demo
    DB_PASSWORD=pve4ic4m.99..aspgf12
    DEBUG=False

    install_project_dependencies
    install_test_dependencies
    create_user_group $USER $GROUP
    create_virtualenv $USER
    provision_test_virtualenv $USER
    copy_and_set_up_django_project $USER $SERVER_NAME $DATA_BASE_NAME $DB_PASSWORD $DEBUG
    create_postgres_db $USER $DATA_BASE_NAME $DB_PASSWORD
    fill_test_db $USER
    set_up_gunicorn_supervisor $USER $GROUP $APP
    set_up_nginx $USER $APP $SERVER_NAME $SERVER_PORT

elif [ "$1" = "stage" ]; then
    PATH_TO_SOURCE_PACKAGE=$2

    USER=alleatstg
    GROUP=webapps
    APP=alleat_staging
    SERVER_NAME=staging.allergicucaneat.com
    SERVER_PORT=80
    DATA_BASE_NAME=sqldb_stage
    DB_PASSWORD=Lz633KjvVjHR75a
    DEBUG=False

    install_project_dependencies
    #install_test_dependencies
    create_user_group $USER $GROUP
    create_virtualenv $USER
    #provision_test_virtualenv $USER
    copy_and_set_up_django_project $USER $SERVER_NAME $DATA_BASE_NAME $DB_PASSWORD $DEBUG $PATH_TO_SOURCE_PACKAGE
    create_postgres_db $USER $DATA_BASE_NAME $DB_PASSWORD
    fill_test_db $USER
    set_up_gunicorn_supervisor $USER $GROUP $APP
    set_up_nginx $USER $APP $SERVER_NAME $SERVER_PORT

else
    echo "Usage: $0 [ build_package | devel | test | demo | stage ]"
fi