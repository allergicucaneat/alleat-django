## Prerrequisites

* Nginx.  
```
apt-get install nginx
```

* Python-virtualenv.  
```
apt-get install python-virtualenv
```


* Supervisor.  
```
apt-get install supervisor
```



## How to deploy

* Tutorial on http://michal.karzynski.pl/blog/2013/06/09/django-nginx-gunicorn-virtualenv-supervisor/.  

#### Supervisor

* Update conf.
```
$ sudo supervisorctl reread
hello: available`
```
```
$ sudo supervisorctl update
hello: added process group`
```

#### Nginx

* Create symbol link.
``
sudo ln -s /etc/nginx/sites-available/butchery-site /etc/nginx/sites-enabled/butchery-site`
```

## Create package

```
pip --target=d:\somewhere\other\than\the\default install package_name

```
