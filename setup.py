import os

from setuptools import setup, find_packages


def read_file(filepath):
    with open(filepath) as f:
        return f.read()


EXCLUDE_FROM_PACKAGES = ['tests', 'tests.api_tests', 'tests.api_tests.test_alleat_api', 'tests.api_tests.util']


setup(
    name='alleat',
    version='0.1',
    description='Alleat Django Project',
    author='AllergicUCanEat Team',
    author_email='ivanprjcts@gmail.com',
    url='https://gitlab.com/allergicucaneat/alleat-django',
    install_requires=read_file('requirements.txt').splitlines(),
    classifiers=[
        'Intended Audience :: Developers',
        'Development Status :: 3 - Alpha',
        'License :: Other/Proprietary License',
        'Framework :: Django :: 1.8',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.7',
    ],
    packages=find_packages(exclude=EXCLUDE_FROM_PACKAGES),
    py_modules=["manage", "db_manage"],
    include_package_data=True,
    package_data={
        'alleat_web_management': [
            'alleat_web_management/static/alleat_web_management/*',
            'alleat_web_management/templates/alleat_web_management/*'
        ],
        'alleat_web_public': [
            'alleat_web_public/static/alleat_web_public/*',
            'alleat_web_public/templates/alleat_web_public/*'
        ],
        'xdjango': [
            'xdjango/static/xdjango/*',
            'xdjango/templates/xdjango/*'
        ]
    },
    zip_safe=False
)