import unittest

from tests.api_tests.util.alleat_api_client import AlleatApi
from tests import settings


class TestApiMenu(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        if settings.USE_PROXY:
            AlleatApi.set_default_proxy("http://%s:%d" % (settings.PROXY_HOST, settings.PROXY_PORT))

        AlleatApi.set_default_host(settings.API_HOST)

        cls.admin_username = settings.ADMIN_USERNAME
        cls.admin_password = settings.ADMIN_PASSWORD

        cls.client_username = settings.CLIENT_USERNAME
        cls.client_password = settings.CLIENT_PASSWORD

        cls.api = AlleatApi()

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        self.api.login(self.admin_username, self.admin_password)
        response = self.api.create_restaurant("New Restaurant")
        self.restaurant_id = response.data["pk"]
        response = self.api.create_item("New Item", self.restaurant_id, "description", 1,
                                          categories=[{"name": "category"}])
        self.category_id = response.data["categories"][0]["pk"]

    def tearDown(self):
        self.api.delete_item(self.category_id)
        self.api.delete_restaurant(self.restaurant_id)
        self.api.logout()

    def test_crud_menu(self):
        """
        Create, read, update and delete a simple item.
        @assert status response is 201
        """
        response = self.api.create_menu("New Menu", self.restaurant_id)
        self.assertEquals(response.status, 201)
        self.assertIn("url", response.data)
        self.assertIn("pk", response.data)
        self.assertEqual(response.data["name"], "New Menu")

        menu_id = response.data["pk"]

        response = self.api.update_menu(menu_id, "New Menu Name", self.restaurant_id)
        self.assertEquals(response.status, 200)
        self.assertEquals(response.data["name"], "New Menu Name")
        self.assertEquals(response.data["pk"], menu_id)
        self.assertIn("url", response.data)

        response = self.api.delete_menu(menu_id)
        self.assertEquals(response.status, 204)

    def test_crud_menu_with_categories(self):
        """
        Create, read, update and delete a simple item with categories.
        @assert status response is 201
        """
        response = self.api.create_menu("New Menu", self.restaurant_id, [self.category_id])
        self.assertEqual(response.status, 201)
        self.assertIn("url", response.data)
        self.assertIn("pk", response.data)
        self.assertNotEqual(response.data["categories"], [])
        self.assertEqual(response.data["name"], "New Menu")

        menu_id = response.data["pk"]

        response = self.api.update_menu(menu_id, "New Menu Name", self.restaurant_id)
        self.assertEqual(response.status, 200)
        self.assertEqual(response.data["name"], "New Menu Name")
        self.assertEqual(response.data["pk"], menu_id)
        self.assertEqual(response.data["categories"], [self.category_id])
        self.assertIn("url", response.data)

        response = self.api.delete_menu(menu_id)
        self.assertEquals(response.status, 204)
