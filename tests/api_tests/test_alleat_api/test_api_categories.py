import unittest

from tests.api_tests.util.alleat_api_client import AlleatApi
from tests import settings


class TestApiCategory(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        if settings.USE_PROXY:
            AlleatApi.set_default_proxy("%s:%d" % (settings.PROXY_HOST, settings.PROXY_PORT))

        AlleatApi.set_default_host(settings.API_HOST)

        cls.admin_username = settings.ADMIN_USERNAME
        cls.admin_password = settings.ADMIN_PASSWORD

        cls.client_username = settings.CLIENT_USERNAME
        cls.client_password = settings.CLIENT_PASSWORD

        cls.api = AlleatApi()

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        self.api.login(self.admin_username, self.admin_password)
        response = self.api.create_restaurant("New Restaurant")
        self.restaurant_id = response.data["pk"]

    def tearDown(self):
        self.api.delete_restaurant(self.restaurant_id)
        self.api.logout()

    def test_crud_category(self):
        """
        Create, read, update and delete a simple category.
        @assert status response is 201
        """
        pass

