import unittest

from tests.api_tests.util.alleat_api_client import AlleatApi
from tests import settings


class TestApiItems(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        if settings.USE_PROXY:
            AlleatApi.set_default_proxy("%s:%d" % (settings.PROXY_HOST, settings.PROXY_PORT))

        AlleatApi.set_default_host(settings.API_HOST)

        cls.admin_username = settings.ADMIN_USERNAME
        cls.admin_password = settings.ADMIN_PASSWORD

        cls.client_username = settings.CLIENT_USERNAME
        cls.client_password = settings.CLIENT_PASSWORD

        cls.api = AlleatApi()

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        self.api.login(self.admin_username, self.admin_password)
        response = self.api.create_restaurant("New Restaurant")
        self.restaurant_id = response.data["pk"]

    def tearDown(self):
        self.api.delete_restaurant(self.restaurant_id)
        self.api.logout()

    def test_crud_item(self):
        """
        Create, read, update and delete a simple item.
        @assert status response is 201
        """
        response = self.api.create_item("New item", self.restaurant_id, "description", 1, None,
                                                     [{"name": "cat"}, {"name": "cat2"}], None, None)
        self.assertEquals(response.status, 201)
        self.assertIn("url", response.data)
        self.assertIn("pk", response.data)

        item_id = response.data["pk"]

        response = self.api.update_item(item_id, "New item name", self.restaurant_id, "description2", 2,
                                                     None, [{"name": "cat"}], None, None)
        self.assertEqual(response.status, 200)
        self.assertEqual(response.data["name"], "New item name")
        self.assertEqual(response.data["categories"][0]["alias"], "cat")
        self.assertIn("url", response.data)
        self.assertIn("pk", response.data)

        response = self.api.delete_item(item_id)
        self.assertEquals(response.status, 204)

    def test_crud_item_with_image(self):
        """
        Create, read, update and delete a simple item.
        @assert status response is 201
        """
        response = self.api.create_item("New item", self.restaurant_id, "description", 1,
                                                     settings.RESOURCES_ROOT + "image.jpg",
                                                     [{"name": "cat"}, {"name": "cat2"}], None, None)
        self.assertEquals(response.status, 201)
        self.assertIn("url", response.data)
        self.assertIn("pk", response.data)

        item_id = response.data["pk"]

        response = self.api.update_item(item_id, "New item name", self.restaurant_id, "description2", 2,
                                                     None, [{"name": "cat"}], None, None)
        self.assertEquals(response.status, 200)
        self.assertEquals(response.data["name"], "New item name")
        self.assertIn("url", response.data)
        self.assertIn("pk", response.data)

        #status, data, headers = self.api.delete_item(item_id)
        #self.assertEquals(status, 204)

    def test_crud_item_partial_update(self):
        """
        Create, read, update and delete a simple item.
        @assert status response is 201
        """
        response = self.api.create_item("New item", self.restaurant_id, "description", 1, None,
                                                     [{"name": "cat"}, {"name": "cat2"}], None, None)
        self.assertEquals(response.status, 201)
        self.assertIn("url", response.data)
        self.assertIn("pk", response.data)

        item_id = response.data["pk"]

        response = self.api.partial_update_item(item_id, None, None, "des3", None)
        self.assertEquals(response.status, 200)
        self.assertEquals(response.data["name"], "New item")
        self.assertEquals(response.data["description"], "des3")
        self.assertIn("url", response.data)
        self.assertIn("pk", response.data)

        response = self.api.delete_item(item_id)
        self.assertEquals(response.status, 204)