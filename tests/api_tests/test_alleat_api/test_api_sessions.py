import unittest

from tests.api_tests.util.alleat_api_client import AlleatApi
from tests import settings


class TestSessionApi(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        if settings.USE_PROXY:
            AlleatApi.set_default_proxy("%s:%d" % (settings.PROXY_HOST, settings.PROXY_PORT))

        AlleatApi.set_default_host(settings.API_HOST)

        cls.username = settings.ADMIN_USERNAME
        cls.password = settings.ADMIN_PASSWORD
        cls.email = settings.ADMIN_EMAIL

        cls.api = AlleatApi()

    @classmethod
    def tearDownClass(cls):
        pass

    def tearDown(self):
        self.api.logout()

    def test_authentication(self):
        """
        Do login, re-login and logout.
        @assert status responses are 200
        @assert data response contains email, phone_number, full_name, pk and url fields
        """
        response = self.api.login(self.username, self.password)
        self.assertEquals(response.status, 200)
        self.assertEquals(response.data["email"], self.email)
        self.assertIn("pk", response.data)
        self.assertIn("url", response.data)

        response = self.api.login(self.username, self.password)
        self.assertEquals(response.status, 200)
        self.assertEquals(response.data["email"], self.email)
        self.assertIn("pk", response.data)
        self.assertIn("url", response.data)

        response = self.api.get_session()
        self.assertEquals(response.status, 200)
        self.assertEquals(response.data["email"], self.email)
        self.assertIn("pk", response.data)
        self.assertIn("url", response.data)

        response = self.api.logout()
        self.assertEquals(response.status, 200)

    def test_get_session_without_being_logged(self):
        """
        Try to get user session without being authenticated.
        @assert status response is 403
        """
        response = self.api.get_session()
        self.assertEquals(response.status, 403)

    def test_authentication_incorrect_email_username(self):
        """
        Try to authenticate using an incorrect email username as username.
        @assert status response is 401
        """
        response = self.api.login("incorrect@username.com", self.password)
        self.assertEquals(response.status, 401)

    def test_authentication_incorrect_phone_number_username(self):
        """
        Try to authenticate using an incorrect phone number as username.
        @assert status response is 401
        """
        response = self.api.login("916676753", self.password)
        self.assertEquals(response.status, 401)

    def test_authentication_incorrect_password(self):
        """
        Try to authenticate using an invalid password.
        @assert status response is 401
        """
        response = self.api.login(self.username, "incorrect_password")
        self.assertEquals(response.status, 401)

    def test_logout_without_being_logged(self):
        """
        Do logout without being logged.
        @assert status response is 200
        """
        response = self.api.logout()
        self.assertEquals(response.status, 403)

    def _test_session_not_allowed_methods(self):
        """
        Try to do PUT, PATCH requests to session api.
        @assert status response is 405
        """
        status, data, headers = self.api._session("PUT")
        self.assertEquals(status, 405)

        status, data, headers = self.api._session("PATCH")
        self.assertEquals(status, 405)

    def _test_session_not_allowed_methods_being_logged(self):
        """
        Try to do PUT, PATCH requests to session api.
        @assert status response is 405
        """
        status, data, headers = self.api.authenticate(self.username, self.password)
        self.assertEquals(status, 200)

        status, data, headers = self.api._session("PUT")
        self.assertEquals(status, 405)

        status, data, headers = self.api._session("PATCH")
        self.assertEquals(status, 405)

        status, data, headers = self.api.logout()
        self.assertEquals(status, 200)