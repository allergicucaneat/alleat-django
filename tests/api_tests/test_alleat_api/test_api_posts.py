import unittest

from tests.api_tests.util.alleat_api_client import AlleatApi
from tests import settings


class TestApiPost(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        if settings.USE_PROXY:
            AlleatApi.set_default_proxy("%s:%d" % (settings.PROXY_HOST, settings.PROXY_PORT))

        AlleatApi.set_default_host(settings.API_HOST)

        cls.admin_username = settings.ADMIN_USERNAME
        cls.admin_password = settings.ADMIN_PASSWORD

        cls.client_username = settings.CLIENT_USERNAME
        cls.client_password = settings.CLIENT_PASSWORD

        cls.api = AlleatApi()

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        self.api.login(self.admin_username, self.admin_password)

    def tearDown(self):
        self.api.logout()

    def test_crud_post(self):
        """
        Create, read, update and delete a simple post.
        """
        response = self.api.create_post(title="New post", body="Body", summary="summary", state=AlleatApi.REVIEW)
        self.assertEquals(response.status, 201)
        self.assertIn("pk", response.data)
        self.assertEqual(response.data["title"], "New post")
        self.assertEqual(response.data["body"], "Body")
        self.assertEqual(response.data["summary"], "summary")
        self.assertEqual(response.data["state"], AlleatApi.REVIEW)

        post_id = response.data["pk"]

        response = self.api.get_post(post_id)
        self.assertEquals(response.status, 200)
        self.assertEqual(response.data["title"], "New post")
        self.assertEqual(response.data["body"], "Body")
        self.assertEqual(response.data["summary"], "summary")
        self.assertEqual(response.data["state"], AlleatApi.REVIEW)

        response = self.api.get_post()
        self.assertEquals(response.status, 200)

        response = self.api.update_post(post_id, title="New post name", body="new Body", summary="new summary",
                                        state=AlleatApi.DRAFT)
        self.assertEquals(response.status, 200)
        self.assertEqual(response.data["title"], "New post name")
        self.assertEqual(response.data["body"], "new Body")
        self.assertEqual(response.data["summary"], "new summary")
        self.assertEqual(response.data["state"], AlleatApi.DRAFT)
        self.assertEquals(response.data["pk"], post_id)

        response = self.api.delete_post(post_id)
        self.assertEquals(response.status, 204)

    def test_crud_post_with_image(self):
        """
        Create, read, update and delete a simple post with image.
        """
        response = self.api.create_post(title="New post", body="Body", summary="summary", state=AlleatApi.REVIEW,
                                        main_photo=settings.RESOURCES_ROOT + "image.jpg")
        self.assertEquals(response.status, 201)
        self.assertIn("pk", response.data)
        self.assertEqual(response.data["title"], "New post")
        self.assertEqual(response.data["body"], "Body")
        self.assertEqual(response.data["summary"], "summary")
        self.assertEqual(response.data["state"], AlleatApi.REVIEW)
        self.assertIsNotNone(response.data["mainPhoto"])

        post_id = response.data["pk"]

        response = self.api.get_post(post_id)
        self.assertEquals(response.status, 200)
        self.assertEqual(response.data["title"], "New post")
        self.assertEqual(response.data["body"], "Body")
        self.assertEqual(response.data["summary"], "summary")
        self.assertEqual(response.data["state"], AlleatApi.REVIEW)
        self.assertIsNotNone(response.data["mainPhoto"])

        response = self.api.get_post()
        self.assertEquals(response.status, 200)

        response = self.api.update_post(post_id, title="New post name", body="new Body", summary="new summary",
                                        state=AlleatApi.DRAFT)
        self.assertEquals(response.status, 200)
        self.assertEqual(response.data["title"], "New post name")
        self.assertEqual(response.data["body"], "new Body")
        self.assertEqual(response.data["summary"], "new summary")
        self.assertEqual(response.data["state"], AlleatApi.DRAFT)
        self.assertEquals(response.data["pk"], post_id)

        response = self.api.delete_post(post_id)
        self.assertEquals(response.status, 204)

    def test_filter_post_by_name(self):
        """
        Filter post by name.
        """
        response = self.api.create_post(title="New post", body="Body", summary="summary", state=AlleatApi.REVIEW)
        self.assertEquals(response.status, 201)
        self.assertIn("pk", response.data)
        self.assertEqual(response.data["title"], "New post")
        self.assertEqual(response.data["body"], "Body")
        self.assertEqual(response.data["summary"], "summary")
        self.assertEqual(response.data["state"], AlleatApi.REVIEW)

        post_id = response.data["pk"]

        response = self.api.get_post(state=AlleatApi.REVIEW)
        self.assertEquals(response.status, 200)
        for rest in response.data["results"]:
            self.assertEqual(AlleatApi.REVIEW, rest["state"])

        response = self.api.delete_post(post_id)
        self.assertEquals(response.status, 204)

    def test_filter_post_by_name_non_existent(self):
        """
        Filter post by state non-existent.
        """
        response = self.api.create_post(title="New post", body="Body", summary="summary", state=AlleatApi.REVIEW)
        self.assertEquals(response.status, 201)
        self.assertIn("pk", response.data)
        self.assertEqual(response.data["title"], "New post")
        self.assertEqual(response.data["body"], "Body")
        self.assertEqual(response.data["summary"], "summary")
        self.assertEqual(response.data["state"], AlleatApi.REVIEW)

        post_id = response.data["pk"]

        response = self.api.get_post(state="non-existent")
        self.assertEqual(response.status, 200)
        self.assertEqual(response.data["results"], [])

        response = self.api.delete_post(post_id)
        self.assertEquals(response.status, 204)
