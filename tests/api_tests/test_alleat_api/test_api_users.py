import unittest

from tests.api_tests.util.alleat_api_client import AlleatApi
from tests import settings


class TestUsersApi(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        if settings.USE_PROXY:
            AlleatApi.set_default_proxy("%s:%d" % (settings.PROXY_HOST, settings.PROXY_PORT))

        AlleatApi.set_default_host(settings.API_HOST)

        cls.admin_username = settings.ADMIN_USERNAME
        cls.admin_password = settings.ADMIN_PASSWORD

        cls.client_username = settings.CLIENT_USERNAME
        cls.client_password = settings.CLIENT_PASSWORD

        cls.api = AlleatApi()

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        response = self.api.login(self.admin_username, self.admin_password)
        self.user_id = response.data["pk"]

    def tearDown(self):
        self.api.logout()

    def test_retrieve_user(self):
        """
        Get logged user detail.
        @assert status responses are 200
        """
        response = self.api.get_user(self.user_id)
        self.assertEquals(response.status, 200)
        self.assertEquals(response.data["email"], self.admin_username)
        self.assertIn("pk", response.data)
        self.assertIn("url", response.data)
        self.assertIn("lastLogin", response.data)
        self.assertIn("isStaff", response.data)
        self.assertIn("isSuperuser", response.data)

    def test_list_users(self):
        """
        Get list of users with only one element, which is the logged user.
        @assert status response is 200
        """
        response = self.api.get_user()
        self.assertEquals(response.status, 200)
        self.assertGreaterEqual(len(response.data), 1)

    def _test_retrieve_user_without_session(self):
        """
        Try to get logged user detail without sending session cookie.
        @assert status responses are 404
        """
        self.api.set_cookie(False)
        status, data, headers = self.api.get_user(self.user_id)
        self.assertEquals(status, 404)

    def _test_list_users_without_session(self):
        """
        Get list of users with no elements, because we do not send any session cookie.
        @assert status response is 200
        @assert response is a empty list
        """
        self.api.set_cookie(False)
        status, data, headers = self.api.get_user()
        self.assertEquals(status, 200)
        self.assertEquals(data, [])

    def test_update_user(self):
        """
        Update user information.
        @assert status response is 200
        """
        response = self.api.update_user(self.user_id, self.admin_username)
        self.assertEquals(response.status, 200)
        self.assertEquals(response.data["email"], self.admin_username)
        self.assertIn("pk", response.data)
        self.assertIn("url", response.data)

    def _test_update_user_without_session(self):
        """
        Try to update user information without session.
        @assert status response is 404
        """
        self.api.set_cookie(False)
        status, data, headers = self.api.update_user(self.user_id, "My name", self.admin_username, "123456789")
        self.assertEquals(status, 404)

    def test_crud_user(self):
        """
        Create, read, update and delete a new user.
        @assert status response is 200
        """
        response = self.api.create_user("userx@123.com")
        self.assertEquals(response.status, 201)
        self.assertIn("pk", response.data)

        user_id = response.data["pk"]

        response = self.api.get_user()
        self.assertEquals(response.status, 200)

        response = self.api.get_user(user_id)
        self.assertEquals(response.status, 200)

        response = self.api.delete_user(user_id)
        self.assertEquals(response.status, 204)

    def test_crud_user_sending_email(self):
        """
        Create, read, update and delete a new user.
        @assert status response is 200
        """
        response = self.api.create_user("ivanmar_91@hotmail.com", send_email=True)
        self.assertEquals(response.status, 201)
        self.assertIn("pk", response.data)

        user_id = response.data["pk"]

        response = self.api.get_user()
        self.assertEquals(response.status, 200)

        response = self.api.get_user(user_id)
        self.assertEquals(response.status, 200)

        response = self.api.delete_user(user_id)
        self.assertEquals(response.status, 204)

    def _test_create_user_with_group_without_session(self):
        """
        Try to create a new user with group without session.
        @assert status response is 401
        """
        self.api.set_cookie(False)
        status, data, _ = self.api.create_user("My name", "userx@123.com", "837465019",
                                               group_name=AlleatApi.ADMINISTRATORS_GROUP)
        self.assertEquals(status, 500)

    def test_crud_user_with_optional_parameters(self):
        """
        Create, read, update and delete a new user with optional parameters.
        @assert status response is 200
        """
        response = self.api.create_user("ivanmar_91@hotmail.com", birthdate="1991-01-06", active_allergens=[])
        self.assertEquals(response.status, 201)
        self.assertIn("pk", response.data)

        user_id = response.data["pk"]

        response = self.api.get_user()
        self.assertEquals(response.status, 200)

        response = self.api.get_user(user_id)
        self.assertEquals(response.status, 200)

        response = self.api.delete_user(user_id)
        self.assertEquals(response.status, 204)