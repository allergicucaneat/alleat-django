import unittest

from tests.api_tests.util.alleat_api_client import AlleatApi
from tests import settings


class TestApiRestaurant(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        if settings.USE_PROXY:
            AlleatApi.set_default_proxy("%s:%d" % (settings.PROXY_HOST, settings.PROXY_PORT))

        AlleatApi.set_default_host(settings.API_HOST)

        cls.admin_username = settings.ADMIN_USERNAME
        cls.admin_password = settings.ADMIN_PASSWORD

        cls.client_username = settings.CLIENT_USERNAME
        cls.client_password = settings.CLIENT_PASSWORD

        cls.api = AlleatApi()

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        self.api.login(self.admin_username, self.admin_password)

    def tearDown(self):
        self.api.logout()

    def test_crud_restaurant(self):
        """
        Create, read, update and delete a simple restaurant.
        @assert status response is 201
        """
        response = self.api.create_restaurant("New Restaurant")
        self.assertEquals(response.status, 201)
        self.assertIn("url", response.data)
        self.assertIn("pk", response.data)
        self.assertEqual(response.data["name"], "New Restaurant")

        rest_id = response.data["pk"]

        response = self.api.get_restaurant(rest_id)
        self.assertEquals(response.status, 200)
        self.assertIn("url", response.data)
        self.assertIn("pk", response.data)
        self.assertEqual(response.data["name"], "New Restaurant")

        response = self.api.get_restaurant()
        self.assertEquals(response.status, 200)

        response = self.api.update_restaurant(rest_id, "New Rest Name")
        self.assertEquals(response.status, 200)
        self.assertEquals(response.data["name"], "New Rest Name")
        self.assertEquals(response.data["pk"], rest_id)
        self.assertIn("url", response.data)

        response = self.api.delete_restaurant(rest_id)
        self.assertEquals(response.status, 204)

    def test_filter_restaurant_by_name(self):
        """
        Filter restaurant by name.
        """
        response = self.api.create_restaurant("New Restaurant")
        self.assertEquals(response.status, 201)
        self.assertIn("url", response.data)
        self.assertIn("pk", response.data)
        self.assertEqual(response.data["name"], "New Restaurant")

        rest_id = response.data["pk"]

        response = self.api.get_restaurant(name="new")
        self.assertEquals(response.status, 200)
        for rest in response.data["results"]:
            self.assertIn("NEW", rest["name"].upper())

        response = self.api.delete_restaurant(rest_id)
        self.assertEquals(response.status, 204)

    def test_filter_restaurant_by_name_non_existent(self):
        """
        Filter restaurant by name non-existent.
        """
        response = self.api.create_restaurant("New Restaurant")
        self.assertEquals(response.status, 201)
        self.assertIn("url", response.data)
        self.assertIn("pk", response.data)
        self.assertEqual(response.data["name"], "New Restaurant")

        rest_id = response.data["pk"]

        response = self.api.get_restaurant(name="non-existent")
        self.assertEquals(response.status, 200)
        self.assertEqual([], response.data["results"])

        response = self.api.delete_restaurant(rest_id)
        self.assertEquals(response.status, 204)

    def test_update_not_existent_restaurant(self):
        """
        Try to update a non-existent restaurant.
        @assert status response is 404
        """
        response = self.api.update_restaurant(321432, "New Rest Name")
        self.assertEquals(response.status, 404)

    def test_get_not_existent_restaurant(self):
        """
        Try to get a non-existent restaurant.
        @assert status response is 404
        """
        response = self.api.get_restaurant(321432)
        self.assertEquals(response.status, 404)

