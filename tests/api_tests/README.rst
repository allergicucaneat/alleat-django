Api Tests
==============


Requirements
------------

- Python 2.7.+.
- sdklib.


Run Tests
---------

1. Change to project directory.

    `cd butchery-django`

2. Run unittests using discover.

    `python -m unittest discover tests.api_tests`