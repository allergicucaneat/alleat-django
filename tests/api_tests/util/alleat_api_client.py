import os
import json
import base64

from sdklib.http import HttpSdk
from sdklib.util.parser import parse_args, safe_add_end_slash
from sdklib.util.files import guess_filename_stream


class AlleatApi(HttpSdk):

    DEFAULT_HOST = "http://allergicucaneat.com"

    API_VERSION = "1.0"

    LOGIN_URL_PATH = "/api/%s/sessions/" % API_VERSION

    API_ITEM_URL = "/api/%s/items/" % API_VERSION
    API_USER_URL = "/api/%s/users/" % API_VERSION
    API_RESTAURANT_URL = "/api/%s/restaurants/" % API_VERSION
    API_MENU_URL = "/api/%s/menus/" % API_VERSION
    API_CATEGORY_URL = "/api/%s/categories/" % API_VERSION
    API_POST_URL = "/api/%s/posts/" % API_VERSION

    ADMINISTRATORS_GROUP = 'administrator'

    DRAFT = "DR"
    REVIEW = "RE"
    PUBLIC = "PU"

    def default_headers(self):
        headers = super(AlleatApi, self).default_headers()
        try:
            headers['X-CSRFToken'] = self.cookie.getcookie()["csrftoken"].value
        except:
            pass
        return headers

    def login(self, username, password):
        return super(AlleatApi, self).login(username=username, password=password)

    def get_session(self):
        return self._http_request("GET", self.LOGIN_URL_PATH)

    def logout(self):
        """
        Close session.
        :return: status, data, headers
        """
        return self._http_request("DELETE", self.LOGIN_URL_PATH)

    def get_item(self, item_id=None):
        """
        Retrieve specified item if order_id is passing, or list all items in other case.
        :param item_id: public key of item
        :return: status, data, headers
        """
        return self._http_request("GET", self.API_ITEM_URL + safe_add_end_slash(item_id))

    def create_item(self, name, restaurant, description, price, main_photo=None, categories=None, allergen_content=None,
                    possibly_allergen_content=None):
        """
        Create a new item.
        :return:
        """
        if main_photo:
            filename, stream = guess_filename_stream(main_photo)
            main_photo = base64.b64encode(stream)
        params = parse_args(name=name, price=price, description=description, categories=categories,
                            allergenContent=allergen_content, possiblyAllergenContent=possibly_allergen_content,
                            restaurant=restaurant, mainPhoto=main_photo)

        return self._http_request("POST", self.API_ITEM_URL, body_params=params)

    def update_item(self, item_id, name, restaurant, description, price, main_photo=None, categories=None,
                    allergen_content=None, possibly_allergen_content=None):
        """
        Update an item.
        :return: status, data, headers
        """
        if main_photo:
            filename, stream = guess_filename_stream(main_photo)
            main_photo = base64.b64encode(stream)
        params = parse_args(name=name, price=price, description=description, categories=categories,
                            allergenContent=allergen_content, possiblyAllergenContent=possibly_allergen_content,
                            restaurant=restaurant, mainPhoto=main_photo)

        return self._http_request("PUT", self.API_ITEM_URL + safe_add_end_slash(item_id), body_params=params)

    def partial_update_item(self, item_id, name, restaurant, description, price, main_photo=None, categories=None,
                            allergen_content=None, possibly_allergen_content=None):
        """
        Update an item.
        :return: status, data, headers
        """
        if main_photo:
            filename, stream = guess_filename_stream(main_photo)
            main_photo = base64.b64encode(stream)
        params = parse_args(name=name, price=price, description=description, categories=categories,
                            allergenContent=allergen_content, possiblyAllergenContent=possibly_allergen_content,
                            restaurant=restaurant, mainPhoto=main_photo)

        return self._http_request("PATCH", self.API_ITEM_URL + safe_add_end_slash(item_id), body_params=params)

    def delete_item(self, item_id):
        """
        Delete an order.
        :param item_id:
        :return:
        """
        return self._http_request("DELETE", self.API_ITEM_URL + safe_add_end_slash(item_id))

    def get_user(self, user_id=None):
        """
        Retrieve specified user if user_id is passing, or list all users in other case.
        :param user_id:
        :return: status, data, headers
        """
        return self._http_request("GET", self.API_USER_URL + safe_add_end_slash(user_id))

    def create_user(self, email, is_staff=True, is_active=True, send_email=False, birthdate=None, active_allergens=None):
        """
        Create a new user.
        :param name:
        :return:
        """
        query_params = parse_args(send_email=send_email)
        params = parse_args(email=email, isStaff=is_staff, isActive=is_active, birthdate=birthdate,
                            activeAllergens=active_allergens)
        return self._http_request("POST", self.API_USER_URL, body_params=params, query_params=query_params)

    def update_user(self, user_id, email, is_staff=True, is_active=True):
        """
        Update an user.
        :param name:
        :return:
        """
        params = parse_args(email=email, isStaff=is_staff, isActive=is_active)
        return self._http_request("PUT", self.API_USER_URL + safe_add_end_slash(user_id), body_params=params)

    def delete_user(self, user_id):
        """
        Delete an user.
        :param name:
        :return:
        """
        return self._http_request("DELETE", self.API_USER_URL + safe_add_end_slash(user_id))

    def get_restaurant(self, restaurant_id=None, name=None):
        """
        Get a restaurant if restaurant_id is passed. Otherwise, get all restaurants.
        :param restaurant_id:
        :return: status, data, headers
        """
        query_params = parse_args(name=name)
        return self._http_request("GET", self.API_RESTAURANT_URL + safe_add_end_slash(restaurant_id),
                                  query_params=query_params)

    def create_restaurant(self, name=None):
        """
        Create a new restaurant.
        :param name:
        :return: status, data, headers
        """
        params = parse_args(name=name)
        return self._http_request("POST", self.API_RESTAURANT_URL, body_params=params)

    def update_restaurant(self, restaurant_id, name):
        """
        Update a restaurant.
        :param restaurant_id:
        :param name:
        :return: status, data, headers
        """
        params = parse_args(name=name)
        return self._http_request("PUT", self.API_RESTAURANT_URL + safe_add_end_slash(restaurant_id), body_params=params)

    def delete_restaurant(self, restaurant_id=None):
        """
        Delete a restaurant.
        :param restaurant_id:
        :return: status, data, headers
        """
        return self._http_request("DELETE", self.API_RESTAURANT_URL + safe_add_end_slash(restaurant_id))

    def create_menu(self, name, restaurant_id, categories=None):
        """
        Create a new menu.
        :param name:
        :return: status, data, headers
        """
        params = parse_args(name=name, restaurant=restaurant_id, categories=categories)
        return self._http_request("POST", self.API_MENU_URL, body_params=params)

    def update_menu(self, menu_id, name, restaurant_id, categories=None):
        """
        Update a menu.
        :param menu_id:
        :param name:
        :return: status, data, headers
        """
        params = parse_args(name=name, restaurant=restaurant_id, categories=categories)
        return self._http_request("PUT", self.API_MENU_URL + safe_add_end_slash(menu_id), body_params=params)

    def delete_menu(self, menu_id=None):
        """
        Delete a menu.
        :param menu_id:
        :return: status, data, headers
        """
        return self._http_request("DELETE", self.API_MENU_URL + safe_add_end_slash(menu_id))

    def get_category(self, category_id=None):
        """
        Get a category if category_id is passed. Otherwise, get all categories.
        :param category_id:
        :return: status, data, headers
        """
        return self._http_request("GET", self.API_CATEGORY_URL + safe_add_end_slash(category_id))

    def create_post(self, title, body, summary, state, main_photo=None):
        """
        Create a post.
        """
        if main_photo:
            filename, stream = guess_filename_stream(main_photo)
            main_photo = base64.b64encode(stream)
        params = parse_args(title=title, body=body, summary=summary, state=state, mainPhoto=main_photo)
        return self._http_request("POST", self.API_POST_URL, body_params=params)

    def get_post(self, post_id=None, state=None):
        """
        Get a post if post_id is passed. Otherwise, get all posts.
        """
        query_params = parse_args(state=state) or None
        return self._http_request("GET", self.API_POST_URL + safe_add_end_slash(post_id),
                                  query_params=query_params)

    def update_post(self, post_id, title, body, summary, state):
        """
        Update a post.
        """
        params = parse_args(title=title, body=body, summary=summary, state=state)
        return self._http_request("PUT", self.API_POST_URL + safe_add_end_slash(post_id), body_params=params)

    def delete_post(self, post_id):
        """
        Delete a post.
        """
        return self._http_request("DELETE", self.API_POST_URL + safe_add_end_slash(post_id))
