from django.conf.urls import patterns, url, include
from django.conf.urls.static import static
from django.contrib import admin
admin.autodiscover()

from alleat_django import settings

# TO_STUDY: Is 'static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)' necessary if using nginx?

urlpatterns = static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + patterns('',

   url(r'^admin/', include(admin.site.urls)),
   url(r'^api/', include('alleat_api.urls')),
   url(r'^management/', include('alleat_web_management.urls', namespace='management', app_name='alleat_web_management')),

   # Put your main app in last place to ensure 'not_found' page is displayed only when it must
   url(r'^', include('alleat_web_public.urls')),

   )
