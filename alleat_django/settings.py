"""
Django settings for butchery_django project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os

BASE_DIR = os.path.dirname(os.path.dirname(__file__))


MEDIA_ROOT = os.path.join(BASE_DIR, 'deploy/media')


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '-8w$_$b+=io340bfs6y3+fnb2v^te=s)_lh+hqwdc5r@!mz8td'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []

LOGIN_REDIRECT_URL = '/'


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
    'alleat_api',
    'alleat_api.models.users',
    'alleat_web_public',
    'alleat_web_management',
    'xdjango',
    'rest_framework',
    'rest_framework_swagger',
    'crispy_forms'
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.locale.LocaleMiddleware',
)

REST_FRAMEWORK = {
    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.AllowAny'
    ],
    'PAGE_SIZE': 20,
    'DEFAULT_VERSIONING_CLASS': 'rest_framework.versioning.NamespaceVersioning',
    'DEFAULT_VERSION': '1.0',
    'ALLOWED_VERSIONS': ['1.0', '1.1'],
    'DEFAULT_RENDERER_CLASSES': (
        'djangorestframework_camel_case.render.CamelCaseJSONRenderer',
        'rest_framework.renderers.BrowsableAPIRenderer',
    ),
    'DEFAULT_PARSER_CLASSES': (
        'djangorestframework_camel_case.parser.CamelCaseJSONParser',
        'rest_framework.parsers.FormParser',
        'rest_framework.parsers.MultiPartParser'
    ),
}

ROOT_URLCONF = 'alleat_django.urls'

WSGI_APPLICATION = 'alleat_django.wsgi.application'

#AUTHENTICATION_BACKENDS = ('xdjango.contrib.auth.backends.AntiBruteForceModelBackend',)

# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}


# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = False

USE_L10N = True

USE_TZ = True

USE_THOUSAND_SEPARATOR = False

# Email settings
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'test.projectx.smtp@gmail.com'
EMAIL_HOST_PASSWORD = 'HgWRakCWyd7FaZHC9vJj8UwKUDPxpE'
EMAIL_USE_TLS = True
DEFAULT_FROM_EMAIL = 'test.projectx.smtp@gmail.com'

# Sms settings
API_SMS_USERNAME = "ivanmar_91@hotmail.com"
API_SMS_KEY = "HdP07kOtOsM-xu5ZlRCbtAL79dSbHTpGAPWO7MjTcY"
DEFAULT_SMS_SENDER = "Carnicerias de Juan"

# ReCaptcha settings
RECAPTCHA_API_KEY = "6Lf9qwgTAAAAAFmFxT8IfBvnAaLWT9GCWHv1LqR7"
RECAPTCHA_SECRET_KEY = "6Lf9qwgTAAAAAPTNnTQ9vtApfO_RM5NadXBooY5a"

LOGIN_URL = '/login'

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'
MEDIA_URL = '/media/'

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'alleat_web_public/static'),
    os.path.join(BASE_DIR, 'alleat_web_management/static'),
    os.path.join(BASE_DIR, 'xdjango/static'),
    os.path.join(BASE_DIR, 'rest_framework/templates'),
    os.path.join(BASE_DIR, 'rest_framework_swagger/templates'),
)

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, 'alleat_web_public/templates'),
    os.path.join(BASE_DIR, 'alleat_web_management/templates'),
    os.path.join(BASE_DIR, 'xdjango/templates'),
)

AUTH_USER_MODEL = 'alleat_api.AUser'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format' : "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            'datefmt' : "%d/%b/%Y %H:%M:%S"
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': 'mysite.log',
            'formatter': 'verbose'
        },
    },
    'loggers': {
        'django': {
            'handlers':['file'],
            'propagate': True,
            'level':'DEBUG',
        },
        'alleat_api': {
            'handlers': ['file'],
            'level': 'DEBUG',
        },
    }
}