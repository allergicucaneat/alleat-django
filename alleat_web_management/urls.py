from django.conf.urls import patterns, url

from alleat_web_management.views import (
   DashboardView, AccountsView, AboutView, ItemView, LoginView, RestaurantView, CategoryView,
   ItemDetailView, MenuDetailView, MenuView, AllergensView,
   DocumentationView, AccountsManagementView, PostDetailView, PostListView
)


urlpatterns = patterns('',

   url(r'^login', LoginView.as_view(), name='login'),
   url(r'^dashboard$', DashboardView.as_view(), name='dashboard'),
   url(r'^restaurants$', RestaurantView.as_view(), name='restaurants'),
   url(r'^categories$', CategoryView.as_view(), name='categories'),
   url(r'^menus$', MenuView.as_view(), name='menus'),
   url(r'^menus/(?P<id>[0-9]+)$', MenuDetailView.as_view()),
   url(r'^new-menu', MenuDetailView.as_view(), name='new-menu'),
   url(r'^items$', ItemView.as_view(), name='items'),
   url(r'^items/(?P<id>[0-9]+)$', ItemDetailView.as_view()),
   url(r'^new-item$', ItemDetailView.as_view(), name='new-item'),
   url(r'^accounts$', AccountsView.as_view(), name='accounts'),
   url(r'^about$', AboutView.as_view(), name='about'),
   url(r'^accounts-management', AccountsManagementView.as_view(), name='accounts-management'),
   url(r'^allergens$', AllergensView.as_view(), name='allergens'),
   url(r'^documentation', DocumentationView.as_view(), name='documentation'),
   url(r'^posts$', PostListView.as_view(), name='posts'),
   url(r'^posts/(?P<id>[0-9]+)$', PostDetailView.as_view()),
   url(r'^new-post$', PostDetailView.as_view(), name='new-post'),
   url(r'^logout$', 'alleat_web_management.views.do_logout', name='logout'),

   url(r'^(.*)$', 'alleat_web_management.views.not_found')

   )
