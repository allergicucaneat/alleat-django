from django.core.urlresolvers import reverse_lazy

from xdjango.contrib.auth.mixins import PermissionRequiredMixin


class AWMPermissionRequiredMixin(PermissionRequiredMixin):
    """
    Alleat Web Management permission required Mixin.
    """
    login_url = reverse_lazy('alleat_web_management:login')


class AWMAdminPanelAccessPermissionRequiredMixin(AWMPermissionRequiredMixin):
    """
    Alleat Web Management mixin for 'admin_panel_access' permission required.
    """
    permission_required = 'alleat_api.admin_panel_access'
