app.service('alleatApi', ['$http', '$cookies', function($http, $cookies) {

    var RESTAURANTS_PATH = "/api/1.0/restaurants/";
    var USERS_PATH = "/api/1.0/users/";
    var CATEGORY_PATH = "/api/1.0/categories/";
    var ITEM_PATH = "/api/1.0/items/";
    var MENU_PATH = "/api/1.0/menus/";
    var ALLERGEN_PATH = "/api/1.0/allergens/";
    var ALLERGEN_STATES_PATH = "/api/1.0/allergen_states";
    var POST_PATH = "/api/1.0/posts/";

    var X_CSRF_TOKEN_HEADER_NAME = "X-CSRFToken";
    var COOKIE_HEADER_NAME = "Cookie";

    var config = {
        headers:  {
            'Content-Type': 'application/json'
        }
    };

    var urlEncode = function(obj) {
        var str = [];
        for(var p in obj)
            if (obj.hasOwnProperty(p)) {
              str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
            }
        return str.join("&");
    };

    var defaultHeaders = function() {
        var csrftoken = $cookies.get("csrftoken") || "";

        var xHeaders = config.headers;
        xHeaders[X_CSRF_TOKEN_HEADER_NAME] = csrftoken;
        return xHeaders;
    };

    var _http = function(method, url, query_params, data, headers, transformRequest) {
        query_params = query_params || null;
        data = data || null;
        headers = headers || defaultHeaders();
        transformRequest = transformRequest || null;

        if (transformRequest != null)
            var req = {
                method: method,
                url: url,
                headers: headers,
                params: query_params,
                data: data,
                transformRequest: transformRequest
            };
        else
            var req = {
                method: method,
                url: url,
                headers: headers,
                params: query_params,
                data: data,
            };

        return $http(req);
    };

    this.getPosts = function() {
        return _http("GET", POST_PATH);
    };

    this.getPost = function(post_id) {
        return _http("GET", POST_PATH + post_id + "/");
    };

    this.createPost = function(title, body, summary, state, mainPhoto) {
        var title = title || null;
        var body = body || null;
        var summary = summary || null;
        var state = state || null;
        var mainPhoto = mainPhoto || null;

        var form_params = {
            'title': title,
            'body': body,
            'summary': summary,
            'state': state,
            'mainPhoto': mainPhoto
        };
        return _http("POST", POST_PATH, null, form_params);
    };

    this.getCreatePostOptions = function() {
        var form_params = {
        };
        return _http("OPTIONS", POST_PATH, null, form_params);
    };

    this.getUpdatePostOptions = function(post_id) {
        var form_params = {
        };
        return _http("OPTIONS", POST_PATH + post_id + "/", null, form_params);
    };

    this.updatePost = function(post_id, title, body, summary, state, mainPhoto) {
        var title = title || null;
        var body = body || null;
        var summary = summary || null;
        var state = state || null;
        var mainPhoto = mainPhoto || null;

        var form_params = {
            'title': title,
            'body': body,
            'summary': summary,
            'state': state,
            'mainPhoto': mainPhoto
        };
        return _http("PUT", POST_PATH + post_id + "/", null, form_params);
    };

    this.deletePost = function(post_id) {
        return _http("DELETE", POST_PATH + post_id + "/", null, null);
    };

    this.getMenus = function() {
        return _http("GET", MENU_PATH);
    };

    this.getUsers = function() {
        return _http("GET", USERS_PATH);
    };

    this.getMenu = function(menu_id) {
        return _http("GET", MENU_PATH + menu_id + "/");
    };

    this.getRestaurants = function(own) {
        own = own || true;
        var url = RESTAURANTS_PATH;
        if (own)
            url += "?own=True";
        return _http("GET", url);
    };

    this.getCategories = function(restaurant_id) {
        var url = CATEGORY_PATH;
        if (restaurant_id != undefined)
            url += "?restaurant_id="+restaurant_id;
        return _http("GET", url);
    };

    this.getItems = function() {
        return _http("GET", ITEM_PATH);
    };

    this.getItem = function(item_id) {
        return _http("GET", ITEM_PATH + item_id + "/");
    };

    this.getAllergens = function() {
        return _http("GET", ALLERGEN_PATH);
    };

    this.getAllergenStates = function() {
        return _http("GET", ALLERGEN_STATES_PATH);
    };

    this.getSession = function() {
        return _http("GET", SESSION_PATH);
    };

    this.authenticate = function(username, password) {
        var form_params = {
            'email_or_phone_number': username,
            'password': password
        };
        return _http("POST", SESSION_PATH, null, form_params);
    };

    this.createUser = function(email) {
        var email = email || null;
        var send_email = true;

        var query_params = {"send_email": send_email}

        var form_params = {
            'email': email,
            'is_staff': true,
            'is_active': true
        };
        return _http("POST", USERS_PATH, query_params, form_params);
    };

    this.createRestaurant = function(name) {
        name = name || null;

        var form_params = {
            'name': name
        };
        return _http("POST", RESTAURANTS_PATH, null, form_params);
    };

    this.updateRestaurant = function(restaurant_id, name) {
        name = name || null;

        var form_params = {
            'name': name
        };
        return _http("PUT", RESTAURANTS_PATH + restaurant_id + "/", null, form_params);
    };

    this.deleteRestaurant = function(restaurant_id) {
        var form_params = {
        };
        return _http("DELETE", RESTAURANTS_PATH + restaurant_id + "/", null, form_params);
    };

    this.updateCategory = function(category_id, name, alias) {
        name = name || null;
        alias = alias || null;

        var form_params = {
            'name': name,
            'alias': alias
        };
        return _http("PUT", CATEGORY_PATH + category_id + "/", null, form_params);
    };

    this.deleteItem = function(item_id) {
        var form_params = {
        };
        return _http("DELETE", ITEM_PATH + item_id + "/", null, form_params);
    };

    this.createMenu = function(name, restaurant_id, categories) {
        name = name || null;
        restaurant_id = restaurant_id || null;
        categories = categories || [];

        var form_params = {
            'name': name,
            'restaurant': restaurant_id,
            'categories': categories
        };

        return _http("POST", MENU_PATH, null, form_params);
    };

    this.updateMenu = function(menu_id, name, restaurant_id, categories) {
        name = name || null;
        restaurant_id = restaurant_id || null;
        categories = categories || [];

        var form_params = {
            'name': name,
            'restaurant': restaurant_id,
            'categories': categories
        };

        return _http("PUT", MENU_PATH + menu_id + "/", null, form_params);
    };

    this.deleteMenu = function(menu_id) {
        var form_params = {
        };
        return _http("DELETE", MENU_PATH + menu_id + "/", null, form_params);
    };

    this.createItem = function(name, description, price, restaurant_id, mainPhoto, categories, allergens, possibly_allergens) {
        name = name || null;
        description = description || null;
        price = price || null;
        restaurant_id = restaurant_id || null;
        mainPhoto = mainPhoto || null;
        categories = categories || [];
        allergens = allergens || [];
        possibly_allergens = possibly_allergens || [];

        var form_params = {
            'mainPhoto': mainPhoto,
            'name': name,
            'description': description,
            'price': price,
            'restaurant': restaurant_id,
            'categories': categories,
            'allergenContent': allergens,
            'possiblyAllergenContent': possibly_allergens
        };

        return _http("POST", ITEM_PATH, null, form_params);
    };

    this.getItem = function(item_id) {
        return _http("GET", ITEM_PATH + item_id + "/");
    };

    this.updateItem = function(item_id, name, description, price, restaurant_id, mainPhoto, categories, allergens,
                               possibly_allergens) {
        name = name || null;
        description = description || null;
        price = price || null;
        restaurant_id = restaurant_id || null;
        mainPhoto = mainPhoto || null;
        categories = categories || [];
        allergens = allergens || [];
        possibly_allergens = possibly_allergens || [];

        var form_params = {
            'mainPhoto': mainPhoto,
            'name': name,
            'description': description,
            'price': price,
            'restaurant': restaurant_id,
            'categories': categories,
            'allergenContent': allergens,
            'possiblyAllergenContent': possibly_allergens
        };
        return _http("PUT", ITEM_PATH + item_id + "/", null, form_params);
    };

}]);