app.controller('CategoriesCtrl', ['$scope', 'alleatApi', function ($scope, alleatApi)
{
    $scope.categories = [];
    $scope.filterCategories = [];
    $scope.restaurantOptions = [];
    $scope.restaurant = {};

    var reloadCategories = function() {
        alleatApi.getCategories().then(function(response) {
            $scope.categories = response.data.results;
            reloadFilterCategories(null);
        });
    };

    var reloadRestaurantOptions = function() {
        alleatApi.getRestaurants().then(function(response) {
            $scope.restaurantOptions = response.data.results;
        });
    };

    $scope.initList = function() {
        reloadCategories();
        reloadRestaurantOptions();
    };

    $scope.saveCategory = function(category_id, name, alias) {
        alleatApi.updateCategory(category_id, name, alias).then(function(response) {

        });
    };

    var reloadFilterCategories = function(restaurant_id) {
        if (restaurant_id == null || restaurant_id == undefined) {
            $scope.filterCategories = $scope.categories;
            return;
        }

        $scope.filterCategories = [];
        for (var i=0; i<$scope.categories.length; i++) {
            if ($scope.categories[i].restaurant == restaurant_id)
                $scope.filterCategories.push($scope.categories[i]);
        }

    };

    $scope.changeSelectedRestaurant = function(restaurant_id) {
        reloadFilterCategories(restaurant_id);
    };


}]);