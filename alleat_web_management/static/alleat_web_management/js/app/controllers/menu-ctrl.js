app.controller('MenuCtrl', ['$scope', 'alleatApi', function ($scope, alleatApi)
{
    $scope.menus = [];
    $scope.filterMenus = [];
    $scope.restaurantOptions = [];
    $scope.menu = {"pk": 0, "restaurant": null, "categories": []};
    $scope.restaurant = {};
    $scope.categories = [];
    $scope.filterCategories = [];


    var reloadMenus = function() {
        alleatApi.getMenus().then(function(response) {
            $scope.menus = response.data.results;
            reloadFilterMenus(null);
        });
    };

    var reloadMenu = function() {
        var pathArray = window.location.pathname.split( '/' );
        var id = pathArray[pathArray.length-1];

        if (id == 'new-menu')
            return;

        alleatApi.getMenu(id).then(function(response) {
            $scope.menu = response.data;
            reloadFilterCategories($scope.menu.restaurant);

        }, function (response) {

        });
    };

    var reloadRestaurantOptions = function() {
        alleatApi.getRestaurants().then(function(response) {
            $scope.restaurantOptions = response.data.results;
        });
    };

    var reloadCategories = function() {
        alleatApi.getCategories().then(function(response) {
            $scope.categories = response.data.results;
            for (var i=0; i<$scope.categories.length; i++) {

                if (isJsonElementInJsonArrayByKeyValue($scope.menu.categories, "pk", $scope.categories[i].pk)) {
                    $scope.categories[i].state = true;
                } else {
                    $scope.categories[i].state = false;
                }
            }
            reloadFilterCategories($scope.menu.restaurant);
        });
    };

    $scope.initDetail = function() {
        reloadMenu();
        reloadRestaurantOptions();
        reloadCategories();
    };

    $scope.initList = function() {
        reloadMenus();
        reloadRestaurantOptions();
    };

    $scope.changeMenu = function(menu_id) {
        // this url should be auto-generated
        document.location.href = "/management/menus/" + menu_id;
    };

    $scope.saveMenu = function() {

        var categories = [];
        for (var i=0; i<$scope.categories.length; i++) {
            if ($scope.categories[i].state)
                categories.push($scope.categories[i].pk)
        }

        if ($scope.menu.pk == 0) {

            alleatApi.createMenu($scope.menu.name, $scope.menu.restaurant, categories).then(function(response) {
                //clearDetails();
            });

        } else {

            alleatApi.updateMenu($scope.menu.pk, $scope.menu.name, $scope.menu.restaurant, categories).then(function(response) {

            });
        }
    };

    var reloadFilterMenus = function(restaurant_id) {
        if (restaurant_id == null || restaurant_id == undefined) {
            $scope.filterMenus = $scope.menus;
            return;
        }

        $scope.filterMenus = [];
        for (var i=0; i<$scope.menus.length; i++) {
            if ($scope.menus[i].restaurant == restaurant_id)
                $scope.filterMenus.push($scope.menus[i]);
        }

    };

    var reloadFilterCategories = function(restaurant_id) {
        if (restaurant_id == null || restaurant_id == undefined) {
            $scope.filterCategories = $scope.categories;
            return;
        }

        $scope.filterCategories = [];
        for (var i = 0; i < $scope.categories.length; i++) {
            if ($scope.categories[i].restaurant == restaurant_id)
                $scope.filterCategories.push($scope.categories[i]);
        }
    };

    $scope.changeSelectedRestaurant = function(restaurant_id) {
        reloadFilterMenus(restaurant_id);
        reloadFilterCategories(restaurant_id);
    };

    $scope.removeMenu = function(menu_id) {
        alleatApi.deleteMenu(menu_id).then(function(response) {
            $scope.menus = removeJsonElementFromJsonArrayByKeyValue($scope.menus, "pk", menu_id);
            reloadMenus();
        });
    };

}]);