app.controller('AccountsManagementCtrl', ['$scope', 'alleatApi', function ($scope, alleatApi)
{
    $scope.email = null;
    $scope.administrators = [];
    

    var sendInvitation = function() {

        alleatApi.createUser($scope.email).then(function(response) {
            $scope.email = null;

        }, function (response) {
            // do nothing
        });
    };
    
    $scope.sendInvitation = function() {
        sendInvitation();
    };

    var loadAdministrators = function() {

        alleatApi.getUsers().then(function(response) {
            $scope.administrators = response.data.results;

        }, function (response) {
            // do nothing
        });
    };

    $scope.initCtrl = function() {
        loadAdministrators();
    };


}]);