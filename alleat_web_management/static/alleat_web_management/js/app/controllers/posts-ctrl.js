app.controller('PostCtrl', ['$scope', '$sce', 'alleatApi', function ($scope, $sce, alleatApi)
{
    $scope.post = {"pk": 0, "mainPhoto": "/static/alleat_web_management/img/Imagen_no_disponible.png"};
    $scope.posts = [];
    $scope.filterPosts = [];
    $scope.postState = [];
    $scope.stateOptions = {};
    $scope.outputImage = null;
    $scope.alerts = [];

    var showSuccess = function() {
        $scope.alerts = [{ type: 'success', msg: 'Bien hecho, ¡guardado con éxito!' }]
    };

    var showError = function() {
        $scope.alerts = [{ type: 'danger', msg: 'Ups... Algo salió mal :(' }];
    };

    $scope.closeAlert = function(index) {
        $scope.alerts.splice(index, 1);
    };

    $scope.dynamicPopover = {
        templateUrl: 'myPopoverTemplate.html',
    };

    var clearDetails = function () {
        $scope.post = {"pk": 0, "mainPhoto": "/static/alleat_web_management/img/Imagen_no_disponible.png"};
    };

    var reloadCreatePostOptions = function() {
        alleatApi.getCreatePostOptions().then(function(response) {
            $scope.stateOptions = response.data.actions.POST.state;
        });
    };

    var reloadUpdatePostOptions = function(post_id) {
        alleatApi.getUpdatePostOptions(post_id).then(function(response) {
            $scope.stateOptions = response.data.actions.PUT.state;
        });
    };

    var reloadPosts = function() {
        alleatApi.getPosts().then(function(response) {
            $scope.posts = response.data.results;
            reloadFilterPosts();
        });
    };

    var reloadPostDetail = function() {
        var pathArray = window.location.pathname.split( '/' );
        var id = pathArray[pathArray.length-1];

        if (id != 'new-post') {

            alleatApi.getPost(id).then(function(response) {
                $scope.post = response.data;
            });
            reloadUpdatePostOptions(id)
            
        } else {
            reloadCreatePostOptions()
        }


    };

    var reloadFilterPosts = function() {
        $scope.filterPosts = $scope.posts;
        console.log($scope.filterPosts)
    };

    $scope.initList = function() {
        reloadCreatePostOptions();
        reloadPosts()
    };

    $scope.initDetail = function() {
        reloadPostDetail();
    };

    $scope.updatePost = function(post_id) {
        // this url should be auto-generated
        document.location.href = "/management/posts/" + post_id;
    };

    $scope.removePost = function(post_id) {
        alleatApi.deletePost(post_id).then(function(response) {
                reloadPosts()
            });
    };

    $scope.savePost = function() {
        var png = $scope.outputImage.split(',')[1];

        if ($scope.post.pk == 0) {

            alleatApi.createPost($scope.post.title, $scope.post.body, $scope.post.summary, $scope.post.state, png)
                .then(function(response) {
                    showSuccess();
                    clearDetails();
            }, function () {
                    showError();
                });

        } else {

            alleatApi.updatePost($scope.post.pk, $scope.post.title, $scope.post.body, $scope.post.summary, $scope.post.state, png)
                .then(function(response) {
                    showSuccess();
            }, function () {
                    showError();
                });
        }
    };

}]);