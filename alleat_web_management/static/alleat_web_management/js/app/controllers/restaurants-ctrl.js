app.controller('RestaurantsCtrl', ['$scope', 'alleatApi', function ($scope, alleatApi)
{
    $scope.restaurants = [];
    $scope.new_restaurant = {};
    $scope.show_new_restaurant = false;

    alleatApi.getRestaurants(true).then(function(response) {
        $scope.restaurants = response.data.results;
    });

    $scope.newRestaurant = function() {
        $scope.show_new_restaurant = true;
    };

    $scope.cancelNewRestaurant = function() {
        $scope.show_new_restaurant = false;
    };

    $scope.saveNewRestaurant = function() {
        alleatApi.createRestaurant($scope.new_restaurant.name).then(function(response) {
            $scope.restaurants.push(response.data)
        });
        $scope.show_new_restaurant = false;
        $scope.new_restaurant = {};
    };

    $scope.saveRestaurant = function(restaurant_id, name) {
        alleatApi.updateRestaurant(restaurant_id, name).then(function(response) {

        });
    };

    $scope.removeRestaurant = function(restaurant_id) {
        alleatApi.deleteRestaurant(restaurant_id, name).then(function(response) {
            $scope.restaurants = removeJsonElementFromJsonArrayByKeyValue($scope.restaurants, "pk", restaurant_id);
        });
    };

    $scope.generateQRCode = function(restaurant_id, qr_code) {
        var blob = new Blob([qr_code], {type: "text/plain;charset=utf-8"});
        saveAs(blob, "qr_code_restaurant" + restaurant_id + ".svg");
    }

}]);