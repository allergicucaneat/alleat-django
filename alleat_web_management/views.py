from django.views.generic import View
from django.shortcuts import render, redirect
from django.contrib.auth import logout, login, authenticate

from alleat_web_management.validators import LoginFormValidator
from alleat_web_management.mixins import AWMAdminPanelAccessPermissionRequiredMixin


class LoginView(View):
    def get(self, request):
        user = request.user
        if user and user.has_perm('alleat_api.admin_panel_access'):
            next_url = request.GET.get('next','/')
            return redirect(next_url)

        context = {

        }
        return render(request, "alleat_web_management/login.html", context)

    def post(self, request):
        """
        Gestiona el login de un usuario.
        """
        return do_login(request, "alleat_web_management/login.html", next="/")


class AdministratorView(AWMAdminPanelAccessPermissionRequiredMixin, View):
    def get(self, request):
        context = {

        }
        return render(request, 'alleat_web_management/dashboard.html', context)


class DashboardView(AWMAdminPanelAccessPermissionRequiredMixin, View):
    """
    Dashboard View Controller class.
    """
    def get(self, request):
        context = {

        }
        return render(request, 'alleat_web_management/dashboard.html', context)


class AboutView(AWMAdminPanelAccessPermissionRequiredMixin, View):
    """
    About View controller class.
    """
    def get(self, request):
        context = {

        }
        return render(request, 'alleat_web_management/about.html', context)


class RestaurantView(AWMAdminPanelAccessPermissionRequiredMixin, View):
    def get(self, request):
        context = {

        }
        return render(request, 'alleat_web_management/restaurants.html', context)


class CategoryView(AWMAdminPanelAccessPermissionRequiredMixin, View):
    def get(self, request):
        context = {

        }
        return render(request, 'alleat_web_management/categories.html', context)


class ItemView(AWMAdminPanelAccessPermissionRequiredMixin, View):
    def get(self, request):
        context = {

        }
        return render(request, 'alleat_web_management/items.html', context)


class ItemDetailView(AWMAdminPanelAccessPermissionRequiredMixin, View):
    def get(self, request, **kwargs):
        context = {

        }
        return render(request, 'alleat_web_management/item_detail.html', context)


class MenuView(AWMAdminPanelAccessPermissionRequiredMixin, View):
    def get(self, request):
        context = {

        }
        return render(request, 'alleat_web_management/menus.html', context)


class MenuDetailView(AWMAdminPanelAccessPermissionRequiredMixin, View):
    def get(self, request, **kwargs):
        context = {

        }
        return render(request, 'alleat_web_management/menu_detail.html', context)


class AllergensView(AWMAdminPanelAccessPermissionRequiredMixin, View):
    def get(self, request):
        context = {

        }
        return render(request, 'alleat_web_management/allergens.html', context)


class DocumentationView(AWMAdminPanelAccessPermissionRequiredMixin, View):
    def get(self, request):
        context = {

        }
        return render(request, 'alleat_web_management/documentation.html', context)


class AccountsView(AWMAdminPanelAccessPermissionRequiredMixin, View):
    """
    Accounts View Controller class.
    """
    def get(self, request):
        context = {
        }
        return render(request, 'alleat_web_management/accounts.html', context)


class AccountsManagementView(AWMAdminPanelAccessPermissionRequiredMixin, View):
    """
    Accounts management Controller class.
    """
    def get(self, request):
        context = {
        }
        return render(request, 'alleat_web_management/accounts_management.html', context)


class PostListView(AWMAdminPanelAccessPermissionRequiredMixin, View):
    """
    Post List View Controller class.
    """
    def get(self, request):
        context = {
        }
        return render(request, 'alleat_web_management/posts.html', context)


class PostDetailView(AWMAdminPanelAccessPermissionRequiredMixin, View):
    """
    Post detail Controller class.
    """
    def get(self, request, **kwargs):
        context = {
        }
        return render(request, 'alleat_web_management/post_detail.html', context)


def do_login(request, template, next="/"):
    """
    Handle user login.
    """
    error_messages = []

    form = LoginFormValidator(request.POST)
    if form.is_valid():
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']

        user = authenticate(username=username, password=password)
        if user is None:
            error_messages.append('Nombre de usuario o contrasena incorrecto')
        else:
            if user.is_active:
                login(request, user)
                next_url = request.GET.get('next', next)
                return redirect(next_url)
            else:
                error_messages.append('El usuario no esta activo')

    context = {
        'errors': error_messages,
        'form_validator': form,
    }
    return render(request, template, context)


def not_found(request, word):
    context = {

    }
    return render(request, 'alleat_web_management/404.html', context)


def do_logout(request):
    logout(request)
    return redirect('/')
