# -*- coding: utf-8 -*-


from django.conf import settings
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _

from alleat_api.models.values import (
    MAX_SHORT_LENGTH, MAX_LONG_LENGTH, CODE_LENGTH,
    CATEGORY_ALIAS_DEFAULT, POST_STATES
)
from xdjango.db.models import SafeManager


@python_2_unicode_compatible
class Allergen(models.Model):
    name = models.CharField(max_length=MAX_SHORT_LENGTH, default="Allergen")
    possibly_allergen_content_description = models.TextField(blank=True)
    allergen_content_description = models.TextField(blank=True)

    objects = SafeManager()

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class Restaurant(models.Model):
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='restaurants', null=True)
    name = models.CharField(max_length=MAX_SHORT_LENGTH)
    is_active = models.BooleanField(_('active'), default=True)

    objects = SafeManager()

    def __str__(self):
        return self.name


class CategoryManager(SafeManager):

    def get_or_create(self, restaurant, name, **kwargs):
        alias = kwargs.pop('alias', name)
        return self.safe_get(restaurant=restaurant, name=name) or self.create(restaurant=restaurant, name=name,
                                                                              alias=alias, **kwargs)

    def safe_clear(self):
        categories = self.all()
        for category in categories:
            category.safe_delete()


@python_2_unicode_compatible
class Category(models.Model):
    name = models.CharField(max_length=MAX_SHORT_LENGTH)
    alias = models.CharField(max_length=MAX_SHORT_LENGTH, default=CATEGORY_ALIAS_DEFAULT)
    restaurant = models.ForeignKey(Restaurant, on_delete=models.CASCADE, related_name='categories')
    is_active = models.BooleanField(_('active'), default=True)

    objects = CategoryManager()

    def __str__(self):
        return self.name

    def safe_delete(self):
        if not self.items.all() or not self.restaurant:
            self.delete()


class Item(models.Model):
    name = models.CharField(max_length=MAX_SHORT_LENGTH)
    description = models.TextField(max_length=MAX_LONG_LENGTH)
    price = models.FloatField()
    main_photo = models.ImageField(upload_to='images/', null=True, blank=True)
    is_active = models.BooleanField(_('active'), default=True)
    allergen_content = models.ManyToManyField(Allergen, related_name='allergen_content', blank=True)
    possibly_allergen_content = models.ManyToManyField(Allergen, related_name='possibly_allergen_content', blank=True)
    restaurant = models.ForeignKey(Restaurant, on_delete=models.CASCADE, related_name='items')
    categories = models.ManyToManyField(Category, blank=True)

    objects = SafeManager()


class Menu(models.Model):
    name = models.CharField(max_length=MAX_SHORT_LENGTH)
    categories = models.ManyToManyField(Category)
    restaurant = models.ForeignKey(Restaurant, on_delete=models.CASCADE, related_name='menus')
    is_active = models.BooleanField(_('active'), default=True)

    objects = SafeManager()


class Post(models.Model):
    title = models.CharField(max_length=MAX_SHORT_LENGTH)
    body = models.CharField(max_length=8000)
    summary = models.CharField(max_length=300)
    state = models.CharField(choices=POST_STATES, max_length=CODE_LENGTH)
    main_photo = models.ImageField(upload_to='images/', null=True, blank=True)
    last_modified = models.DateTimeField(auto_now=True)

    objects = SafeManager()

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['-last_modified']
