# -*- coding: utf-8 -*-


from django.contrib.auth.models import Group

from alleat_api.models import Allergen, Post
from alleat_api.models.values import PUBLIC_STATE


def get_group_instance(name):
    try:
        return Group.objects.get(name=name)
    except:
        return Group.objects.create(name=name)


def update_allergen_db():
    Allergen.objects.get_or_create(name="Gluten")
    Allergen.objects.get_or_create(name=u"Crustáceos")
    Allergen.objects.get_or_create(name="Huevos")
    Allergen.objects.get_or_create(name="Pescado")
    Allergen.objects.get_or_create(name="Cacahuetes")
    Allergen.objects.get_or_create(name="Soja")
    Allergen.objects.get_or_create(name="Leche")
    Allergen.objects.get_or_create(name=u"Frutos de Cáscara")
    Allergen.objects.get_or_create(name="Apio")
    Allergen.objects.get_or_create(name="Mostaza")
    Allergen.objects.get_or_create(name=u"Sésamo")
    Allergen.objects.get_or_create(name="Sulfitos")
    Allergen.objects.get_or_create(name="Altramuces")
    Allergen.objects.get_or_create(name="Moluscos")


def update_post_db():
    Post.objects.get_or_create(
        title="Entrada de ejemplo",
        body="""Veggies es bonus vobis, proinde vos postulo essum magis kohlrabi welsh onion daikon amaranth tatsoi tomatillo melon azuki bean garlic. Gumbo beet greens corn soko endive gumbo gourd. Parsley shallot courgette tatsoi pea sprouts fava bean collard greens dandelion okra wakame tomato. Dandelion cucumber earthnut pea peanut soko zucchini.\n\n ### Subtitle \n\nTurnip greens yarrow ricebean rutabaga endive cauliflower sea lettuce kohlrabi amaranth water spinach avocado daikon napa cabbage asparagus winter purslane kale. Celery potato scallion desert raisin horseradish spinach carrot soko. Lotus root water spinach fennel kombu maize bamboo shoot green bean swiss chard seakale pumpkin onion chickpea gram corn pea. Brussels sprout coriander water chestnut gourd swiss chard wakame kohlrabi beetroot carrot watercress. Corn amaranth salsify bunya nuts nori azuki bean chickweed potato bell pepper artichoke. Nori grape silver beet broccoli kombu beet greens fava bean potato quandong celery. Bunya nuts black-eyed pea prairie turnip leek lentil turnip greens parsnip. Sea lettuce lettuce water chestnut eggplant winter purslane fennel azuki bean earthnut pea sierra leone bologi leek soko chicory celtuce parsley jÃ­cama salsify. Celery quandong swiss chard chicory earthnut pea potato. Salsify taro catsear garlic gram celery bitterleaf wattle seed collard greens nori. Grape wattle seed kombu beetroot horseradish carrot squash brussels sprout chard.""",
        summary="Veggies es bonus vobis, proinde vos postulo essum magis kohlrabi welsh onion daikon amaranth tatsoi tomatillo melon azuki bean garlic.",
        state=PUBLIC_STATE
    )
