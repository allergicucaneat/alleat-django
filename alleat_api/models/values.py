# -*- coding: utf-8 -*-


GLUTEN = 'G'
CRUSTACEANS = 'C'
EGGS = 'E'
FISH = 'F'
PEANUTS = 'P'
SOY = 'S'
MILK = 'M'
NUTS = 'N'
CELERY = 'c'
MUSTARD = 'm'
SESAME = 's'
SULPHITES = 'SS'
LUPINS = 'L'
MOLLUSCS = 'ML'
ALLERGENS = (
    (GLUTEN,'Gluten'),
    (CRUSTACEANS,u'Crustáceos'),
    (EGGS, 'Huevos'),
    (FISH, 'Pescados'),
    (PEANUTS, 'Cacahuetes'),
    (SOY, 'Soja'),
    (MILK, 'Leche'),
    (NUTS, u'Frutos de cáscara'),
    (CELERY, 'Apio'),
    (MUSTARD, 'Mostaza'),
    (SESAME, u'Sésamo'),
    (SULPHITES, 'Sulfitos'),
    (LUPINS, 'Altramuces'),
    (MOLLUSCS, 'Moluscos'),
)

CONTENT_ALLERGEN = 'CA'
POSSIBLY_CONTENT_ALLERGEN = 'PA'
NO_ALLERGEN = 'NA'
ALLERGEN_STATES = (
    (NO_ALLERGEN, u'No contiene el alérgeno'),
    (POSSIBLY_CONTENT_ALLERGEN, u'Puede contener el alérgeno'),
    (CONTENT_ALLERGEN, u'Contiene el alérgeno'),
)

DRAFT_STATE = 'DR'
PUBLIC_STATE = 'PU'
TO_REVIEW_STATE = 'RE'
POST_STATES = (
    (DRAFT_STATE, u'Borrador'),
    (TO_REVIEW_STATE, u'Para revisión'),
    (PUBLIC_STATE, u'Público'),
)


ADMINISTRATOR_GROUP_NAME = 'administrator'
GROUPS = (
    (ADMINISTRATOR_GROUP_NAME,'Administrator'),
)

ORDER_MESSAGE_MAX_VALUE = 300
ADDRESS_MAX_LENGTH = 200
CITY_MAX_LENGTH = 40
MAX_SHORT_LENGTH = 100
MAX_LONG_LENGTH = 300
CODE_LENGTH = 2
FULL_NAME_MAX_LENGTH = 50
NAME_MAX_LENGTH = 20

CATEGORY_ALIAS_DEFAULT = "Category"