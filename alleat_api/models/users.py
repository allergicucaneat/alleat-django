from django.db import models

from xdjango.contrib.auth.models import AbstractEmailUser, EmailUserManager
from xdjango.db.models import SafeManager

from alleat_api.models import Allergen


class AllergenProfileManager(SafeManager):
    def get_or_create(self, **kwargs):
        is_enabled = kwargs.pop('is_enabled', False)
        super(AllergenProfileManager, self).get_or_create(is_enabled=is_enabled, **kwargs)


class AUser(AbstractEmailUser):
    """
    Concrete class of AbstractUser.
    """
    objects = EmailUserManager()

    birthdate = models.fields.DateField(null=True)
    active_allergens = models.ManyToManyField(Allergen, blank=True)

    class Meta(AbstractEmailUser.Meta):
        swappable = 'AUTH_USER_MODEL'

        permissions = (
            ("admin_panel_access", "Can access to alleat admin panel"),
        )
