
from django.conf.urls import patterns, url, include

from rest_framework import routers

from alleat_api.views import (
   ItemViewSet, RestaurantViewSet, MenuViewSet, CategoryViewSet, AllergenViewSet,
   AllergenStateListAPIView, PostViewSet, AUserEmailViewSet
)


router = routers.DefaultRouter()
router.register(r'items', ItemViewSet)
router.register(r'restaurants', RestaurantViewSet)
router.register(r'menus', MenuViewSet)
router.register(r'categories', CategoryViewSet)
router.register(r'allergens', AllergenViewSet)
router.register(r'posts', PostViewSet)
router.register(r'users', AUserEmailViewSet)


urlpatterns = patterns('',

   url(r'^1.0/auth/', include('rest_framework.urls', namespace='rest_framework')),

   url(r'^1.0/', include(router.urls, namespace='1.0')),
   #url(r'^', include(router.urls, namespace=settings.REST_FRAMEWORK.get("DEFAULT_VERSION", None))),
   #url(r'^1.1/', include(router.urls, namespace='1.1')),
   url(r'^1.0/allergen_states$', AllergenStateListAPIView.as_view()),

   url(r'^1.0/', include('xdjango.urls')),

   url(r'^docs/', include('rest_framework_swagger.urls')),

   )
