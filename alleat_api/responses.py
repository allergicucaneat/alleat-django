from rest_framework.response import Response
from rest_framework import status


FORM_NOT_VALID_ERROR_MSG = "Form is not valid"
QUERY_NOT_VALID_ERROR_MSG = "Query is not valid"
INVALID_CREDENTIALS_ERROR_MSG = "Invalid credentials"
USER_NOT_AUTHENTICATED_ERROR_MSG = "User is not authenticated"
THIS_FIELD_IS_REQUIRED_ERROR_MSG = "This field is required."


def http_200_ok(message=None):
    if message:
        data = message
        return Response(status=status.HTTP_200_OK, data=data)
    return Response(status=status.HTTP_200_OK)


def http_201_created(message=None):
    if message:
        data = message
        return Response(status=status.HTTP_201_CREATED, data=data)
    return Response(status=status.HTTP_201_CREATED)


def http_400_bad_request(message=None):
    if message:
        data = message
        return Response(status=status.HTTP_400_BAD_REQUEST, data=data)
    return Response(status=status.HTTP_400_BAD_REQUEST)


def http_401_not_authorized(message=None):
    if message:
        data = message
        return Response(status=status.HTTP_401_UNAUTHORIZED, data=data)
    return Response(status=status.HTTP_401_UNAUTHORIZED)


def http_403_forbidden(message=None):
    if message:
        data = message
        return Response(status=status.HTTP_403_FORBIDDEN, data=data)
    return Response(status=status.HTTP_403_FORBIDDEN)


def http_404_not_found(message=None):
    if message:
        data = message
        return Response(status=status.HTTP_404_NOT_FOUND, data=data)
    return Response(status=status.HTTP_404_NOT_FOUND)