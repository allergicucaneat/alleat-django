from django.forms import (
    CharField, FloatField, ImageField, Form, MultipleChoiceField, BooleanField, ChoiceField, ModelChoiceField,
    ModelMultipleChoiceField
)

from alleat_api.models.values import (
    ALLERGENS, GROUPS
)
from alleat_api.models import Restaurant, Category
from xdjango.forms.fields import MultiCharField
from xdjango.forms.validators import (
    EmailOrPhoneNumberFormValidator
)


class ItemFormValidator(Form):
    name = CharField()
    description = CharField()
    price = FloatField(min_value=0.01)
    main_photo = ImageField(required=False)
    allergen_content = MultipleChoiceField(choices=ALLERGENS, required=False)
    possibly_allergen_content = MultipleChoiceField(choices=ALLERGENS, required=False)
    categories = MultiCharField(required=False)
    restaurant = ModelChoiceField(queryset=Restaurant.objects.all())


class MenuFormValidator(Form):
    name = CharField()
    restaurant = ModelChoiceField(queryset=Restaurant.objects.all())
    categories = ModelMultipleChoiceField(queryset=Category.objects.all(), required=False)


class CategoryUpdateFormValidator(Form):
    name = CharField()


class CategoryCreateFormValidator(CategoryUpdateFormValidator):
    restaurant = ModelChoiceField(queryset=Restaurant.objects.all())


class ClientFormValidator(EmailOrPhoneNumberFormValidator):
    full_name = CharField(required=False)
    password = CharField(required=False)
    group_name = ChoiceField(choices=GROUPS, required=False)


class PasswordFormValidator(Form):
    old_password = CharField(required=True)
    new_password = CharField(required=True)


class ClientWithPasswordFormValidator(ClientFormValidator):
    password = CharField()


class LoginFormValidator(Form):
    username = CharField()
    password = CharField()


class PasswordResetFormValidator(EmailOrPhoneNumberFormValidator):
    new_password = CharField()
    token = CharField()


class EditClientFormValidator(Form):
    cod_perm = BooleanField(required=False)
    client_id = CharField()
