import io

import pyqrcode


def generate_qr_code(url):
    qr_code = pyqrcode.create(url)
    buffer = io.BytesIO()
    qr_code.svg(buffer, scale=4, module_color="#7D007D")
    return buffer.getvalue()