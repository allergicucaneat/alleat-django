from django.db.models import Q
from rest_framework import viewsets, filters
from rest_framework.mixins import (
    RetrieveModelMixin, ListModelMixin, UpdateModelMixin
)
from rest_framework.response import Response
from rest_framework.views import APIView

from xdjango.contrib.auth.mixins import SecureGetObjectMixin
from xdjango.contrib.auth.views import UserEmailViewSet

from alleat_api.filters import RestaurantFilter, PostFilter
from alleat_api.models.values import ALLERGEN_STATES
from alleat_api.models import (
    Item, Category, Allergen, Restaurant, Menu, Post
)
from alleat_api.permissions import (
    IsRestaurantOwnerOrReadActiveOnly, IsOwnerOrReadActiveOnly
)
from alleat_api.serializers import (
    ItemSerializer, CategorySerializer, RestaurantSerializer, MenuSerializer, AllergenSerializer,
    SimpleItemSerializer, MenuLargeSerializer, PostSerializer, AUserSerializer
)
from alleat_api.pagination import PageNnumberParamPagination


class ItemViewSet(SecureGetObjectMixin, viewsets.ModelViewSet):
    """
    API endpoint that allows items to be viewed or edited.
    """
    queryset = Item.objects.all()
    serializer_class = ItemSerializer
    permission_classes = (IsRestaurantOwnerOrReadActiveOnly,)

    def get_queryset(self):
        queryset = self.queryset

        user = self.request.user
        if user.is_authenticated():
            restaurants = Restaurant.objects.filter(owner=user)
            queryset = queryset.filter(Q(restaurant__in=restaurants) | Q(is_active=True))
        else:
            queryset = queryset.filter(is_active=True)

        return queryset

    def get_serializer_class(self):
        if hasattr(self.request, 'version') and self.request.version == '1.1':
            return SimpleItemSerializer
        return ItemSerializer


class CategoryViewSet(SecureGetObjectMixin, UpdateModelMixin, RetrieveModelMixin, ListModelMixin,
                      viewsets.GenericViewSet):
    """
    API endpoint that allows categories to be viewed.
    """
    # queryset modified by get_queryset()
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    permission_classes = (IsRestaurantOwnerOrReadActiveOnly,)

    def get_queryset(self):
        queryset = self.queryset

        user = self.request.user
        if user.is_authenticated():
            restaurants = Restaurant.objects.filter(owner=user)
            queryset = queryset.filter(Q(restaurant__in=restaurants) | Q(is_active=True))
        else:
            queryset = queryset.filter(is_active=True)

        restaurant_id = self.request.query_params.get('restaurant_id', None)
        if restaurant_id:
            restaurant = Restaurant.objects.safe_get(pk=restaurant_id)
            if restaurant:
                queryset = queryset.filter(restaurant=restaurant)

        return queryset


class RestaurantViewSet(SecureGetObjectMixin, viewsets.ModelViewSet):
    """
    API endpoint that allows manage restaurant objects.
    """
    queryset = Restaurant.objects.all()
    serializer_class = RestaurantSerializer
    permission_classes = (IsOwnerOrReadActiveOnly,)
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = RestaurantFilter

    def get_queryset(self):
        """
        Optionally restricts the returned restaurants to a given user,
        by filtering against a `name` query parameter in the URL.
        """
        queryset = self.queryset

        user = self.request.user
        if user.is_authenticated():
            queryset = queryset.filter(Q(owner=user) | Q(is_active=True))
        else:
            queryset = queryset.filter(is_active=True)

        own = self.request.query_params.get('own', False)
        if own and self.request.user.is_authenticated:
            queryset = queryset.filter(owner=self.request.user)
        return queryset

    def perform_create(self, serializer):
        if self.request.user.is_authenticated():
            serializer.save(owner=self.request.user)


class MenuViewSet(SecureGetObjectMixin, viewsets.ModelViewSet):
    """
    API endpoint that allows menus to be viewed or edited.
    """
    queryset = Menu.objects.all()
    serializer_class = MenuSerializer
    permission_classes = (IsRestaurantOwnerOrReadActiveOnly,)

    def get_queryset(self):
        queryset = self.queryset

        user = self.request.user
        if user.is_authenticated():
            restaurants = Restaurant.objects.filter(owner=user)
            queryset = queryset.filter(Q(restaurant__in=restaurants) | Q(is_active=True))
        else:
            queryset = queryset.filter(is_active=True)

        return queryset

    def retrieve(self, request, *args, **kwargs):

        menu = self.get_object()
        context={'request': request}
        serializer = MenuLargeSerializer(menu, context=context)
        return Response(serializer.data)

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        context={'request': request}

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = MenuLargeSerializer(page, many=True, context=context)
            return self.get_paginated_response(serializer.data)

        serializer = MenuLargeSerializer(queryset, many=True, context=context)
        return Response(serializer.data)


class AllergenViewSet(SecureGetObjectMixin, ListModelMixin, RetrieveModelMixin, viewsets.GenericViewSet):
    """
    API endpoint that allows allergens to be viewed.
    """
    queryset = Allergen.objects.all()
    serializer_class = AllergenSerializer


class AllergenStateListAPIView(APIView):
    """
    List all packaging methods.
    """
    queryset = Allergen.objects.all()  # Required for DjangoModelPermissions

    def get(self, request, format=None):
        data = []
        for code, name in ALLERGEN_STATES:
            data.append(
                {
                    "code": code,
                    "displayed_name": name
                }
            )
        return Response(data)


class PostViewSet(SecureGetObjectMixin, viewsets.ModelViewSet):
    """
    API endpoint that allows post to be viewed or edited.
    """
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = PostFilter
    pagination_class = PageNnumberParamPagination


class AUserEmailViewSet(UserEmailViewSet):
    """
    API endpoint that allows users to be created, viewed or edited.
    """
    serializer_class = AUserSerializer
