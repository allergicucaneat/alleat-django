# -*- coding: utf-8 -*-

from drf_extra_fields.fields import Base64ImageField
from rest_framework import serializers
from xdjango.contrib.auth.serializers import UserSerializer

from alleat_api.controllers import generate_qr_code
from alleat_api.models import Item, Allergen, Category, Menu, Restaurant, Post


class AllergenSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Allergen
        fields = ('url', 'pk', 'name', 'possibly_allergen_content_description', 'allergen_content_description')


class SimpleItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Item
        fields = ('name', 'possibly_allergen_content', 'allergen_content')


class SimpleRestaurantSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Restaurant
        fields = ('pk', 'name')


class SimpleCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('pk', 'name', 'alias')


class SimpleMenuSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Menu
        fields = ('url', 'pk', 'name')


class RestaurantSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Restaurant
        fields = ('url', 'pk', 'name', 'is_active')

    def to_representation(self, instance):
        ret = super(RestaurantSerializer, self).to_representation(instance)
        menus = Menu.objects.filter(restaurant=instance)
        serializer = SimpleMenuSerializer(menus, many=True, context=self.context)
        ret["menus"] = serializer.data
        ret["qr_code"] = generate_qr_code(ret["url"])
        return ret


class CategorySerializer(serializers.HyperlinkedModelSerializer):
    #items = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    restaurant = serializers.ReadOnlyField(source='restaurant.pk')

    class Meta:
        model = Category
        fields = ('url', 'pk', 'name', 'alias', 'restaurant', 'is_active')

    def to_representation(self, instance):
        ret = super(CategorySerializer, self).to_representation(instance)
        items = Item.objects.filter(categories__in=[instance])
        serializer = SimpleItemSerializer(items, many=True)
        ret["items"] = serializer.data
        return ret


class ItemSerializer(serializers.HyperlinkedModelSerializer):
    restaurant = serializers.PrimaryKeyRelatedField(queryset=Restaurant.objects.all())
    main_photo = Base64ImageField(allow_null=True, required=False)
    categories = SimpleCategorySerializer(many=True, required=False)
    allergen_content = serializers.PrimaryKeyRelatedField(many=True, queryset=Allergen.objects.all(), required=False)
    possibly_allergen_content = serializers.PrimaryKeyRelatedField(many=True, queryset=Allergen.objects.all(),
                                                                   required=False)

    def get_fields(self, *args, **kwargs):
        fields = super(ItemSerializer, self).get_fields(*args, **kwargs)
        if "view" in self.context and self.context['view'].request.user.is_authenticated():
            fields['restaurant'].queryset = Restaurant.objects.filter(owner=self.context['view'].request.user)
        else:
            fields['restaurant'].queryset = Restaurant.objects.none()
        return fields

    class Meta:
        model = Item
        fields = ('url', 'pk', 'name', 'description', 'price', 'main_photo', 'is_active', 'allergen_content',
                  'possibly_allergen_content', 'restaurant', 'categories')

    def create(self, validated_data):
        categories_data = validated_data.pop('categories', [])
        allergen_content_data = validated_data.pop('allergen_content', [])
        possibly_allergen_content_data = validated_data.pop('possibly_allergen_content', [])
        restaurant_data = validated_data.get('restaurant')
        item = Item.objects.create(**validated_data)
        for allergen_data in allergen_content_data:
            item.allergen_content.add(allergen_data)
        for allergen_data in possibly_allergen_content_data:
            item.possibly_allergen_content.add(allergen_data)
        for category_data in categories_data:
            category = Category.objects.get_or_create(restaurant=restaurant_data, **category_data)
            item.categories.add(category)
        return item

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.description = validated_data.get('description', instance.description)
        instance.price = validated_data.get('price', instance.price)
        instance.main_photo = validated_data.get('main_photo', instance.main_photo)
        instance.is_active = validated_data.get('is_active', instance.is_active)
        restaurant_data = validated_data.get('restaurant', instance.restaurant)
        instance.restaurant = restaurant_data
        instance.save()

        categories_data = validated_data.get('categories', None)
        if categories_data is not None:
            instance.categories.clear()
            for category_data in categories_data:
                category = Category.objects.get_or_create(restaurant=restaurant_data, **category_data)
                instance.categories.add(category)
        allergen_content_data = validated_data.get('allergen_content', None)
        if allergen_content_data is not None:
            instance.allergen_content.clear()
            for allergen_data in allergen_content_data:
                instance.allergen_content.add(allergen_data)
        possibly_allergen_content_data = validated_data.get('possibly_allergen_content', None)
        if possibly_allergen_content_data is not None:
            instance.possibly_allergen_content.clear()
            for allergen_data in possibly_allergen_content_data:
                instance.possibly_allergen_content.add(allergen_data)
        return instance


class MenuSerializer(serializers.HyperlinkedModelSerializer):
    categories = serializers.PrimaryKeyRelatedField(queryset=Category.objects.all(), many=True, required=False)
    restaurant = serializers.PrimaryKeyRelatedField(queryset=Restaurant.objects.all())

    def get_fields(self, *args, **kwargs):
        fields = super(MenuSerializer, self).get_fields(*args, **kwargs)
        if "view" in self.context and self.context['view'].request.user.is_authenticated():
            fields['restaurant'].queryset = Restaurant.objects.filter(owner=self.context['view'].request.user)
            fields['categories'].queryset = Category.objects.filter(restaurant__in=fields['restaurant'].queryset)
        else:
            fields['restaurant'].queryset = Restaurant.objects.none()
            fields['categories'].queryset = Category.objects.none()
        return fields

    def validate(self, data):
        """
        Check that all category restaurants are equal to restaurant.
        """
        categories_data = data.get('categories', [])
        restaurant_data = data.get('restaurant', None)
        for category in categories_data:
            if restaurant_data != category.restaurant:
                raise serializers.ValidationError("all categories must belong to restaurant")
        return data

    class Meta:
        model = Menu
        fields = ('url', 'pk', 'name', 'categories', 'restaurant', 'is_active')


class MenuLargeSerializer(serializers.HyperlinkedModelSerializer):
    categories = CategorySerializer(read_only=True, many=True)
    restaurant = serializers.PrimaryKeyRelatedField(queryset=Restaurant.objects.all())

    class Meta:
        model = Menu
        fields = ('url', 'pk', 'name', 'categories', 'restaurant', 'is_active')


class PostSerializer(serializers.ModelSerializer):
    main_photo = Base64ImageField(allow_null=True, required=False)

    class Meta:
        model = Post
        fields = ('pk', 'title', 'body', 'summary', 'state', 'main_photo', 'last_modified')


class AUserSerializer(UserSerializer):
    #active_allergens = AllergenSerializer(many=True)

    class Meta(UserSerializer.Meta):
        fields = UserSerializer.Meta.fields + ('birthdate', 'active_allergens')

    def update(self, instance, validated_data):
        super(AUserSerializer, self).update(instance, validated_data)
        instance.birthdate = validated_data.get('birthdate', instance.birthdate)
        instance.save()
        return instance
