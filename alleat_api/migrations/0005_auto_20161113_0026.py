# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('alleat_api', '0004_auto_20161113_0024'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='last_modified',
            field=models.DateTimeField(default=datetime.datetime(2016, 11, 13, 0, 26, 53, 330283, tzinfo=utc), auto_now=True),
            preserve_default=False,
        ),
    ]
