# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('alleat_api', '0011_auto_20161122_1719'),
    ]

    operations = [
        migrations.AlterField(
            model_name='auser',
            name='active_allergens',
            field=models.ManyToManyField(to='alleat_api.Allergen', null=True),
        ),
    ]
