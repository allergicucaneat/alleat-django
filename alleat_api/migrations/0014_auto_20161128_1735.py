# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('alleat_api', '0013_auto_20161128_1735'),
    ]

    operations = [
        migrations.AlterField(
            model_name='auser',
            name='active_allergens',
            field=models.ManyToManyField(to='alleat_api.Allergen', blank=True),
        ),
    ]
