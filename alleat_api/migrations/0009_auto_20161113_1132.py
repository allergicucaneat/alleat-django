# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('alleat_api', '0008_auto_20161113_1130'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='post',
            options={'ordering': ['last_modified']},
        ),
    ]
