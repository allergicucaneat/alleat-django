# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('alleat_api', '0003_auto_20160912_1717'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='auser',
            name='allergens',
        ),
        migrations.RemoveField(
            model_name='auser',
            name='birth_date',
        ),
        migrations.AddField(
            model_name='post',
            name='last_modified',
            field=models.DateTimeField(auto_now=True, null=True),
        ),
    ]
