# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('alleat_api', '0010_auto_20161113_1136'),
    ]

    operations = [
        migrations.AddField(
            model_name='auser',
            name='active_allergens',
            field=models.ManyToManyField(to='alleat_api.Allergen'),
        ),
        migrations.AddField(
            model_name='auser',
            name='birthdate',
            field=models.DateField(null=True),
        ),
    ]
