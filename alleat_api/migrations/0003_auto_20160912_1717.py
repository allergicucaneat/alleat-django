# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('alleat_api', '0002_post_main_photo'),
    ]

    operations = [
        migrations.AddField(
            model_name='auser',
            name='allergens',
            field=models.ManyToManyField(to='alleat_api.Allergen'),
        ),
        migrations.AddField(
            model_name='auser',
            name='birth_date',
            field=models.DateField(null=True),
        ),
    ]
