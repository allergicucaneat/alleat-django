from django.contrib import admin

from alleat_api.models.users import AUser
from alleat_api.models import Allergen, Post


admin.site.register(AUser)
admin.site.register(Allergen)
admin.site.register(Post)
