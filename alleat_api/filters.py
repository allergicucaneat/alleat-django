import django_filters

from rest_framework import filters

from alleat_api.models import Restaurant, Post


class RestaurantFilter(filters.FilterSet):
    name = django_filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = Restaurant
        fields = ['name']


class PostFilter(filters.FilterSet):
    class Meta:
        model = Post
        fields = ['state']
