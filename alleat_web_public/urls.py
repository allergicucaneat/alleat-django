from django.conf.urls import patterns, url

from alleat_web_public.views import (
    HomeView, PostDetailView, PostsView, SignUpView
)


urlpatterns = patterns('',
    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^posts/(?P<id>[0-9]+)/$', PostDetailView.as_view(), name='post-detail'),
    url(r'^posts$', PostsView.as_view(), name='posts'),
    url(r'^sign-up', SignUpView.as_view(), name='sign-up'),

    url(r'^(.*)$', 'alleat_web_public.views.not_found')

   )
