app.controller('HomeCtrl', ['$scope', function ($scope)
{
    $scope.rows = [];
    $scope.restaurants = [];
    $scope.searchName = "";

    var calculateRows = function (items) {
        var toReturn = [];
        var nRows = Math.ceil(items.length/2);
        for (var i=0; i<nRows; i++) {
            var col1 = items[i*2];
            var col2 = null;
            if (items.length > i*2+1) {
                col2 = items[i*2+1]
            }
            toReturn.push({"col1": col1, "col2": col2});
        }
        return toReturn;
    };

    var getRestaurantsContainName = function (restaurants, part_name) {
        var toReturn = [];
        for (var i=0; i<restaurants.length; i++) {
            if (restaurants[i].name.toLowerCase().includes(part_name.toLowerCase())) {
                toReturn.push(restaurants[i])
            }
        }
        return toReturn;
    };

    $scope.initPage = function() {
        var item1 = {"pk": 0, "name": "Restaurante 1"};
        var item2 = {"pk": 0, "name": "Restaurante 2"};
        var item3 = {"pk": 0, "name": "Restaurante 3"};
        var item4 = {"pk": 0, "name": "Restaurante 4"};
        var item5 = {"pk": 0, "name": "Restaurante 5"};
        var item6 = {"pk": 0, "name": "Restaurante 6"};
        $scope.restaurants = [item1, item2, item3, item4, item5];

        $scope.rows = calculateRows($scope.restaurants);
    };

    $scope.updateSearch = function() {
        var restaurants = getRestaurantsContainName($scope.restaurants, $scope.searchName);
        $scope.rows = calculateRows(restaurants);
    };

}]);