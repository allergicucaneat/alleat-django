app.controller('PostDetailCtrl', ['$scope', 'alleatApi', function ($scope, alleatApi)
{
  $scope.post = {};
  $scope.lastPosts = [];


  $scope.initPage = function() {
    var pathArray = window.location.pathname.split( '/' );
    var id = pathArray[pathArray.length-2];

    alleatApi.getPost(id).then(function(response) {
      $scope.post = response.data;
    });

    alleatApi.getPosts(3).then(function(response) {
      $scope.lastPosts = response.data.results;
    });

  };

}]);