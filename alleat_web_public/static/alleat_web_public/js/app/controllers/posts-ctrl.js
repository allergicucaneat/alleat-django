app.controller('PostsCtrl', ['$scope', 'alleatApi', function ($scope, alleatApi)
{
  $scope.posts = [];
  $scope.totalItems = 6;
  $scope.currentPage = 1;
  $scope.previousPage = 1;
  $scope.nextPage = 1;

  $scope.setPage = function (pageNo) {
    $scope.currentPage = pageNo;
  };

  $scope.pageChanged = function() {
    //$log.log('Page changed to: ' + $scope.currentPage);
  };

  $scope.initPage = function() {
    alleatApi.getPosts().then(function(response) {
      $scope.posts = response.data.results;
    });
  };

  $scope.updateSearch = function() {
    var restaurants = getRestaurantsContainName($scope.restaurants, $scope.searchName);
    $scope.rows = calculateRows(restaurants);
  };

}]);