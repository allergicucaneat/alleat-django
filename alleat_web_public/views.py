from django.shortcuts import render
from django.views.generic import View

class ConstructionView(View):
    """
    Construction Controller class.
    """
    def get(self, request, **kwargs):
        context = {
        }
        return render(request, 'alleat_web_public/index_construction.html', context)


class HomeView(View):
    """
    Home Controller class.
    """
    def get(self, request, **kwargs):
        context = {
        }
        return render(request, 'alleat_web_public/index.html', context)


class PostsView(View):
    """
    Posts Controller class.
    """
    def get(self, request, **kwargs):
        context = {
        }
        return render(request, 'alleat_web_public/posts.html', context)


class PostDetailView(View):
    """
    Post detail Controller class.
    """
    def get(self, request, **kwargs):
        context = {
        }
        return render(request, 'alleat_web_public/post_detail.html', context)


class SignUpView(View):
    """
    Sign Up Controller class.
    """
    def get(self, request, **kwargs):
        context = {
        }
        return render(request, 'alleat_web_public/sign_up.html', context)


def not_found(request, word):
    context = {

    }
    return render(request, 'alleat_web_public/404.html', context)
