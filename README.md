# Alleat Django app #

[![Deploy Status](http://gitlab:52867e5486ad951d50b5937385933eb5@jenkins.ivanprojects.com:8080/view/alleat/job/alleat_stage_deploy/badge/icon)](http://gitlab:52867e5486ad951d50b5937385933eb5@jenkins.ivanprojects.com:8080/view/alleat/job/alleat_stage_deploy/)
[![Build Status](http://gitlab:52867e5486ad951d50b5937385933eb5@jenkins.ivanprojects.com:8080/view/alleat/job/alleat_stage_build/badge/icon)](http://gitlab:52867e5486ad951d50b5937385933eb5@jenkins.ivanprojects.com:8080/view/alleat/job/alleat_stage_build/)


## Description ##

This repository is composed by 3 main django applications grouped in a master django APP called `alleat_django`:
* `alleat_api`: alleat REST API backend.
* `alleat_web_management`: internal management web application for administrators.
* `alleat_web_public`: public web application for all users.

Also, there are some extra applications integrated in this project:
* `admin`: internal data base management web application for super admin.
* `rest_framework_swagger`: browsable API documentation for developers.

Finally, some integrated libraries:
* `django_rest_framework`: third-party library for REST API implementation.
* `xdjango`: own library for reusing generic `django` and `django_rest_framework` code.


## Documentation ##

* API docs can be read in `/api/docs/` url path.


## Development requirements ##

* Python 2.7.+. [Download](https://www.python.org/downloads/)
* Git. [Windows download](https://sourceforge.net/projects/gitextensions/) [[+Help](http://codetunnel.io/installing-git-on-windows/)] or [SourceTree](https://www.sourcetreeapp.com/)
* Virtualbox 4.3.+. [Download](https://www.virtualbox.org/) - [Older releases](https://www.virtualbox.org/wiki/Download_Old_Builds)
* Vagrant 1.8.+. [Download](https://www.vagrantup.com/)
* Pycharm (IDE recommended). [Download](https://www.jetbrains.com/pycharm/)


## First steps ##

### Ensure you have added a ssh key to your settings ###

* If it is the first time you are using git, configure your git settings (from command line):
```
git config --global user.name "Your Name"
git config --global user.email "your_email@whatever.com"
```

* Generate ssh key from command line [[+Help](http://docs.gitlab.com/ce/ssh/README.html)]. 
```
ssh-keygen
cat $HOME/.ssh/id_rsa.pub
```
* Copy ssh key to your gitlab account [account settings](https://gitlab.com/profile/keys). 


### Download source code ###

* Clone repository from command line. 
```
git clone git@gitlab.com:allergicucaneat/alleat-django.git
```

### Create virtualenv ###


## Setting up ##

* Open a command line, and from root directory, where you can see a `Vagrantfile`, run vagrant up and provision. 
    - Switch on VM.
    ```
    vagrant up
    ```
    - Provision VM.
    ```
    vagrant provision
    ```
* App will be running on your browser in the url `http://devel.allergicucaneat.com/`.
* User: admin@123.com, Password: 123
* Finally, switch off VM.
```
vagrant halt
```